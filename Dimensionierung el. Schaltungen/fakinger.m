clear all; close all;
set(0, 'defaultlinelinewidth', 1.2);

% Read the CSV file
data = readtable('NewFile1.csv', 'Delimiter', ',');
data = table2array(data(:, 1:3));

figure;
plot(wdenoise(data(:, 3), 1), wdenoise(data(:,2), 1));
xticks(0:15);
yticks(0:15);
grid; xlim([5, 15]);
xlabel('input voltage [V]');
ylabel('output voltage [V]');


figure;
plot(data(:, 1)*1e-4, wdenoise((data(:, 3)), 1)); hold on;
plot(data(:, 1)*1e-4, wdenoise((data(:, 2)), 1));
xlabel('time [s]');
ylabel('voltage [V]');
legend('input voltage', 'output voltage');
grid;

% Read the CSV file
data = readtable('NewFile2.csv', 'Delimiter', ',');
data = table2array(data(:, 1:3));

figure;
plot(data(:, 1)*1e-6, wdenoise((data(:, 3)), 1)); hold on;
plot(data(:, 1)*1e-6, wdenoise((data(:, 2)), 1));
xlabel('time [s]');
ylabel('voltage [V]');
legend('input voltage', 'output voltage');
grid;

% Read the CSV file
data = readtable('NewFile3.csv', 'Delimiter', ',');
data = table2array(data(:, 1:3));

figure;
plot(data(:, 1)*1e-7, wdenoise((data(:, 3)), 1)); hold on;
plot(data(:, 1)*1e-7, wdenoise((data(:, 2)), 1));
xlabel('time [s]');
ylabel('voltage [V]');
legend('input voltage', 'output voltage');
grid;


% Read the CSV file
data = readtable('NewFile4.csv', 'Delimiter', ',');
data = table2array(data(:, 1:3));

figure;
plot(data(:, 1)*1e-9, wdenoise((data(:, 3)), 1)); hold on;
plot(data(:, 1)*1e-9, wdenoise((data(:, 2)), 1));
xlabel('time [s]');
ylabel('voltage [V]');
legend('input voltage', 'output voltage');
grid;

