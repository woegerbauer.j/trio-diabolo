#Filename: HW3_kNN_skeleton.py
#Author: Harald Leisenberger
#Edited: May, 2023

import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import scipy.stats as stats
import math
import random
from sklearn.datasets import load_diabetes


plt.rcParams.update({
    "text.usetex": True,
    "font.family": "Helvetica"
})

#--------------------------------------------------------------------------------
# Assignment 3 - Section 2 (k-Nearest Neighbors)
#--------------------------------------------------------------------------------

def main():    
    
    
    # !!! All undefined functions should be implemented in the section 'Helper Functions' !!!
    
    
    # 2.1 kNN for Classification
    # ----------------------------------------------
    
    # Load the two data arrays (training set: 120 x 3 - array, test set: 80 x 3 - array)
    # Column 1: feature 1; Column 2: feature 2; Column 3: class label (0, 1 or 2)
    data_training = np.loadtxt('HW3_kNN_training.data')
    data_test = np.loadtxt('HW3_kNN_test.data')
    
    X_train = data_training[:,0:2]
    t_train = data_training[:,2]
    X_test = data_test[:,0:2]
    t_test = data_test[:,2]
 
    # TODO: Use the function kNN_classifyer to visualize the decision boundaries based on the training data
    #       for k=1,2,3,4,5.
    
    # count2 = 0
    # ks = np.arange(1,6)
    # score = np.zeros([len(ks),1])
    
    # for k in ks:
    #     eta = 0.01
    #     my = 0.2
    #     x1 = np.arange(np.min(X_test[:,0])-my,np.max(X_train[:,0])+my,eta)
    #     x2 = np.arange(np.min(X_test[:,1])-my,np.max(X_train[:,1])+my,eta)
        
    #     count = 0
    #     cl = np.zeros([len(x1),len(x2)])
    #     for x in x1:
    #         X_new = np.array([x*np.ones(len(x2)),x2])
    #         X_new = np.transpose(X_new)
    #         print(count)
    #         cl[count,:] = kNN_classifyer(X_train, t_train, 3, X_new, k)
    #         count += 1
        
    #     plt.figure()
    #     plt.contourf(x1,x2,np.transpose(cl))
        
    #     c = ['violet' if samp == 0 else 'b' if samp == 1 else 'y' for samp in t_train]
    #     plt.scatter(data_training[:,0],data_training[:,1],c=c,edgecolors='k')
    #     plt.xlabel('$x_{1}$')
    #     plt.ylabel('$x_{2}$')
    #     plt.title('Decision boundaries and training data, $k =$ '+str(k))
        
    #     y_estimate = kNN_classifyer(X_train, t_train, 3, X_test, k)    
    #     score[count2] = kNN_score(t_test, y_estimate)
        
    #     c = ['violet' if samp == 0 else 'b' if samp == 1 else 'y' for samp in t_test]
        
    #     plt.figure()
    #     plt.contourf(x1,x2,np.transpose(cl))
    #     plt.scatter(data_test[:,0],data_test[:,1],c=c,edgecolors='k')
    #     plt.xlabel('$x_{1}$')
    #     plt.ylabel('$x_{2}$')
    #     plt.title('Decision boundaries and test data, $k =$ '+str(k)+'; score of '+str(score[count2].item())+'$\%$')
    #     count2 += 1
    # print(score)
    
    # TODO: Use the kNN_score to compute the classification score on the test data for k=1,2,3,4,5 and plot
    #       the results.
    
    # ks = np.arange(1,21)
    # N = 2000
    # score = np.zeros([N,len(ks)])
    
    # for n in range(N):
    #     count = 0
    #     for k in ks:
    #         y_estimate = kNN_classifyer(X_train, t_train, 3, X_test, k)
    #         score[n,count] = kNN_score(t_test, y_estimate)
    #         count += 1
    #     print(n)
    
    # mean_score = np.mean(score, axis=0)
    # plt.figure()
    # plt.plot(ks,mean_score)
    # plt.xlabel('$k$')
    # plt.ylabel('Score in $\%$')
    # plt.title('Mean Score after ' + str(N) + ' realizations')
    # TODO: Compute the test score for $k=1,2,...,20$ and plot it against k.
    
    
    # 2.2 kNN for Regression (Bonus)
    # ----------------------------------------------
    
    diabetes = load_diabetes()
    blood_pressure_all = diabetes.data[:,3]
    blood_pressure = blood_pressure_all[0:40]
    diabetes_value = diabetes.target[0:40]
    
    
    # TODO: Use the function two_NN_regression to fit a function that predicts the diabetes targets in dependence of
    #       the blood pressure on the interval [-0.1,0.1] and vizualize the results.
    
    X_new = np.arange(-0.1,0.1,0.005)
    y_estimate = two_NN_regression(blood_pressure, diabetes_value, X_new)
    
    plt.figure()
    plt.plot(X_new,y_estimate)
    idx = np.argsort(blood_pressure)
    plt.plot(blood_pressure[idx],diabetes_value[idx])
    plt.xlabel('x_1')
    plt.ylabel('x_2')
    #plt.legend(['estimates','targets'])
    
    pass
     
#--------------------------------------------------------------------------------
# Helper Functions (to be implemented!)
#--------------------------------------------------------------------------------


def kNN_classifyer(X_train,t_train,nr_classes,X_new,k):
    
    """ Applies k-Nearest-Neighbors to predict the value of new data based
        on the training data. 
    
    Input: X_train ... training features
            t_train ... training classes
            nr_classes ... number of classes
            X_new ... new, unseen data to be classified
            k ... number of neighbors to be taken into account for classifying new data
           
    Output: y_estimate ... estimated classes of all new data points
    """
    
    y_estimate = np.zeros(len(X_new))
    
    # TODO: Implement kNN for a general k and a general number of classes
    
    for n in range(len(X_new)):
        x = X_new[n,:]
        dist = np.sqrt((x[0]-X_train[:,0])**2+(x[1]-X_train[:,1])**2)
        idx = np.argsort(dist)
        tsort = t_train[idx]
        tsort = tsort[0:k]
        unique_elements, counts = np.unique(tsort, return_counts=True)
        most_common_index = choose_entry(counts)
        y_estimate[n] = unique_elements[most_common_index]
        
    return y_estimate

#--------------------------------------------------------------------------------

def kNN_score(t_test,y_estimate):
    
    """ Evaluates the percentage of correctly classified data points on a test set 
    
    Input: t_test ... true classes of test samples
           y_estimate ... kNN-estimated classes of test samples
           
    Output: y_estimate ... estimated classes of all new data points
    """
    
    # TODO: implement the score evaluation function for kNN
    error = t_test-y_estimate
    error_count = np.flatnonzero(error)
    score = (1-len(error[error_count])/len(t_test))*100
    
    return score

#--------------------------------------------------------------------------------

def two_NN_regression(X_train,t_train,X_new):
    
    """ Applies 2-Nearest-Neighbors to predict the targets of new data based
        on the training data.
    
    Input: X_train ... training features
           t_train ... training targets
           X_new ... new, unseen data whose targets are to be estimated
           k ... number of neighbors to be taken into account for classifying new data
           
    Output: y_estimate ... estimated classes of all new data points
    """
    
    y_estimate = np.zeros(len(X_new))
    
    # TODO: Implement 2NN for a 1-dimensional regression problem
    
    for n in range(len(X_new)):
        x = X_new[n]
        dist = np.sqrt((x-X_train)**2)
        idx = np.argsort(dist)
        tsort = t_train[idx]
        distsort = dist[idx]
        mindist = np.min(distsort)
        y_estimate[n] = (tsort[0]*mindist/distsort[0] + tsort[1]*mindist/distsort[1])/2
        
    return y_estimate

#--------------------------------------------------------------------------------

def choose_entry(entries):
    maximum = max(entries)
    max_entries = [index for index, entry in enumerate(entries) if entry == maximum]
    
    if len(max_entries) == 1:
        return max_entries[0]
    else:
        return random.choice(max_entries)

#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
