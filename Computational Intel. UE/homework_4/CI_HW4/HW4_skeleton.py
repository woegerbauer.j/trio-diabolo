# Filename: HW4_skeleton.py
# Edited: June 2023

import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal
from sklearn import datasets


# --------------------------------------------------------------------------------

# Assignment 4

def main():
    # ------------------------
    # (0) Get the input
    ## (a) load the modified iris data
    data, labels, feature_names = load_iris_data()

    ## (b) construct the datasets
    x_2dim = data[:, [0, 2]]
    x_4dim = data

    # TODO: implement PCA (Required for working on 4.1!)
    # x_2dim_pca = PCA(data,nr_dimensions=2,whitening=False)

    ## (c) visually inspect the data with the provided function (see example below)
    # plot_iris_data(x_2dim,labels, feature_names[0], feature_names[2], "Iris Dataset")

    # ------------------------
    # Scenario 1: Consider a 2-dim slice of the data and evaluate the EM and the k-Means algorithm. Corresponds to scenario 1 for both algorithms and involves 2.1 and 3.1!
    scenario = 1
    dim = 2
    nr_components = 2

    # TODO set parameters
    tol = 0.001  # tolerance
    max_iter = 200  # maximum iterations for GN
    nr_components = 3 #n number of components
    
    #TODO: implement
    #... = EM(x_2dim,nr_components, alpha_0, mean_0, cov_0, max_iter, tol)

    (alpha_0, mean_0, cov_0) = init_EM(dimension = dim, nr_components= nr_components, scenario=scenario, X=x_2dim)

    (alpha, mean, cov, log_likelihood, labels_EM) = EM(x_2dim,nr_components, alpha_0, mean_0, cov_0, max_iter, tol)
    
    label_mat = np.zeros((150,2))
    label_mat[:, 0] = labels
    labels_EM = labels_EM[:,0]
    new_labels = reassign_class_labels(labels_EM)
    label_mat[:, 1] = labels_EM
    
    c = ['blue' if samp == new_labels[0] else 'orange' if samp == new_labels[1] else 'green' if samp == new_labels[2] else 'violet' if samp == 3 else 'red' for samp in labels_EM]
    fig1 = plt.figure(figsize=(12, 8))
    sub1 = fig1.add_subplot(1,2,1)
    sub1.scatter(x_2dim[:,0],x_2dim[:,1],c=c,edgecolors='none')
    plt.xlabel('$x_{1}$')
    plt.ylabel('$x_{2}$')
    plt.title('EM labeled')
    
    c = ['blue' if samp == 0 else 'orange' if samp == 1 else 'green' for samp in labels]
    sub2 = fig1.add_subplot(1,2,2)
    sub2.scatter(x_2dim[:,0],x_2dim[:,1],c=c,edgecolors='none')
    plt.xlabel('$x_{1}$')
    plt.ylabel('$x_{2}$')
    plt.title('Originally labeled')
    # plt.figure()
    # plt.contour(norm_test)
    
    fig2 = plt.figure(figsize=(12,8))
    c = ['blue' if samp == new_labels[0] else 'orange' if samp == new_labels[1] else 'green' if samp == new_labels[2] else 'violet' if samp == 3 else 'red' for samp in labels_EM]
    plt.scatter(x_2dim[:,0],x_2dim[:,1],c=c,edgecolors='none')
    plt.xlabel('$x_{1}$')
    plt.ylabel('$x_{2}$')
    plt.title('EM labeled')
    for k in range(nr_components):
        plot_gauss_contour(mean[:,k], cov[:,:,k], np.min(x_2dim[:,0]), np.max(x_2dim[:,0]),  np.min(x_2dim[:,1]), np.max(x_2dim[:,1]), 100)
    
    #initial_centers = init_k_means(dimension = dim, nr_cluster=nr_components, scenario=scenario)
    #... = k_means(x_2dim, nr_components, initial_centers, max_iter, tol)

    #TODO visualize your results
    fig3 = plt.figure(figsize=(12,8))
    iterations = np.arange(0,log_likelihood.shape[0],1)
    plt.plot(iterations, log_likelihood)
    plt.xlabel("iterations")
    plt.ylabel("log likelihood")

    # TODO: implement
    # (alpha_0, mean_0, cov_0) = init_EM(dimension = dim, nr_components= nr_components, scenario=scenario)
    # ... = EM(x_2dim,nr_components, alpha_0, mean_0, cov_0, max_iter, tol)
    '''
    initial_centers = init_k_means(dimension=dim, nr_clusters=nr_components, scenario=scenario, X=x_2dim)
    centers, cum_dist, labels_c = k_means(x_2dim, nr_components, initial_centers, max_iter, tol)

    plt.grid()
    plt.plot(np.linspace(0, len(cum_dist)-1, len(cum_dist)), cum_dist)
    plt.xlabel('iterations')
    plt.ylabel('cumulative distance')
    plt.savefig('cum_dist_split_init.pdf')
    plt.show()

    plt.grid()

    plt.scatter(x_2dim[labels_c == 0, 0], x_2dim[labels_c == 0, 1])
    plt.scatter(x_2dim[labels_c == 1, 0], x_2dim[labels_c == 1, 1])
    plt.scatter(x_2dim[labels_c == 2, 0], x_2dim[labels_c == 2, 1])
    #plt.scatter(x_2dim[labels_c == 3, 0], x_2dim[labels_c == 3, 1])
    #plt.scatter(x_2dim[labels_c == 4, 0], x_2dim[labels_c == 4, 1])
    plt.scatter(centers[:, 0], centers[:, 1], marker='x', s=150, c='k')
    plt.xlabel('sepal length')
    plt.ylabel('sepal width')
    plt.legend(['label 0', 'label 1', 'label 2', 'label 3', 'label 4', 'means'])
    #plt.title('k-means' + str((np.sum(labels==labels_c)/len(labels))))
    #plt.savefig('kmeans_5.pdf')
    plt.show()
    #TODO visualize your results
    '''

    # ------------------------
    # Scenario 2: Consider the full 4-dimensional data and evaluate the EM and the k-Means algorithm. Corresponds to scenario 2 for both algorithms and involves Sec.2.2 and Sec.3.2!
    scenario = 2
    dim = 4
    nr_components = 3

    tol = 0.001  # tolerance
    max_iter = 200  # maximum iterations for GN
    nr_components = 3 #n number of components
    
    (alpha_0, mean_0, cov_0) = init_EM(dimension = dim, nr_components= nr_components, scenario=scenario, X=x_4dim)

    (alpha, mean, cov, log_likelihood, labels_EM) = EM(x_4dim,nr_components, alpha_0, mean_0, cov_0, max_iter, tol)
    
    label_mat = np.zeros((150,2))
    label_mat[:, 0] = labels
    labels_EM = labels_EM[:,0]
    new_labels = reassign_class_labels(labels_EM)
    label_mat[:, 1] = labels_EM

    c = ['blue' if samp == new_labels[0] else 'orange' if samp == new_labels[1] else 'green' if samp == new_labels[2] else 'violet' if samp == 3 else 'red' for samp in labels_EM]
    fig1 = plt.figure(figsize=(12, 8))
    sub1 = fig1.add_subplot(1,2,1)
    sub1.scatter(x_2dim[:,0],x_2dim[:,1],c=c,edgecolors='none')
    plt.xlabel('$x_{1}$')
    plt.ylabel('$x_{2}$')
    plt.title('EM labeled')
    
    c = ['blue' if samp == 0 else 'orange' if samp == 1 else 'green' for samp in labels]
    sub2 = fig1.add_subplot(1,2,2)
    sub2.scatter(x_2dim[:,0],x_2dim[:,1],c=c,edgecolors='none')
    plt.xlabel('$x_{1}$')
    plt.ylabel('$x_{2}$')
    plt.title('Originally labeled')
    # plt.figure()
    # plt.contour(norm_test)

    #TODO visualize your results
    fig3 = plt.figure(figsize=(12,8))
    iterations = np.arange(0,log_likelihood.shape[0],1)
    plt.plot(iterations, log_likelihood)
    plt.xlabel("iterations")
    plt.ylabel("log likelihood")
    
    (alpha, mean, cov, log_likelihood, labels_EM) = EM(x_4dim,nr_components, alpha_0, mean_0, cov_0, max_iter, tol, diag=1)
    
    c = ['blue' if samp == new_labels[0] else 'orange' if samp == new_labels[1] else 'green' if samp == new_labels[2] else 'violet' if samp == 3 else 'red' for samp in labels_EM]
    fig1 = plt.figure(figsize=(12, 8))
    sub1 = fig1.add_subplot(1,2,1)
    sub1.scatter(x_2dim[:,0],x_2dim[:,1],c=c,edgecolors='none')
    plt.xlabel('$x_{1}$')
    plt.ylabel('$x_{2}$')
    plt.title('EM labeled')
    
    c = ['blue' if samp == 0 else 'orange' if samp == 1 else 'green' for samp in labels]
    sub2 = fig1.add_subplot(1,2,2)
    sub2.scatter(x_2dim[:,0],x_2dim[:,1],c=c,edgecolors='none')
    plt.xlabel('$x_{1}$')
    plt.ylabel('$x_{2}$')
    plt.title('Originally labeled')
    
    #TODO visualize your results
    fig3 = plt.figure(figsize=(12,8))
    iterations = np.arange(0,log_likelihood.shape[0],1)
    plt.plot(iterations, log_likelihood)
    plt.xlabel("iterations")
    plt.ylabel("log likelihood")

    # TODO: implement
    # (alpha_0, mean_0, cov_0) = init_EM(dimension = dim, nr_components= nr_components, scenario=scenario)
    # ... = EM(x_2dim, nr_components, alpha_0, mean_0, cov_0, max_iter, tol)
    '''
    initial_centers = init_k_means(dimension=dim, nr_clusters=nr_components, scenario=1, X=x_4dim)
    centers, cum_dist, labels_c = k_means(x_4dim, nr_components, initial_centers, max_iter, tol)

    accuracy = np.sum(labels_c == labels)/len(labels)
    plt.grid()
    plt.scatter(x_2dim[labels_c == 0, 0], x_2dim[labels_c == 0, 1])
    plt.scatter(x_2dim[labels_c == 1, 0], x_2dim[labels_c == 1, 1])
    plt.scatter(x_2dim[labels_c == 2, 0], x_2dim[labels_c == 2, 1])
    plt.scatter(centers[:, 0], centers[:, 2], marker='x', s=150, c='k')
    plt.xlabel('sepal length')
    plt.ylabel('sepal width')
    plt.legend(['label 0', 'label 1', 'label 2', 'means'])
    plt.title('4 dimensional k-means, accuracy: ' + str(accuracy))
    plt.savefig('kmeans_4dim.pdf')
    plt.show()

    plt.grid()
    plt.plot(np.linspace(0, len(cum_dist) - 1, len(cum_dist)), cum_dist)
    plt.xlabel('iterations')
    plt.ylabel('cumulative distance')
    plt.savefig('cum_dist_4dim.pdf')
    plt.show()'''

    # TODO: visualize your results by looking at the same slice as in 1)

    # ------------------------
    # Scenario 3: Perform PCA to reduce the dimension from 4 to 2 while preserving most of the variance. Corresponds to scenario 3 for both algorithms and involves Sec.4.1!
    # Evaluate the EM and the k-Means algorithm on the transformed data.
    scenario = 3
    dim = 2
    nr_components = 3

    # TODO set parameters
    tol = 1e-2  # tolerance
    max_iter = 1000  # maximum iterations for GN
    nr_components = 3  # n number of components

    x_2dim_pca, explained_variance = PCA(x_4dim, 2, True)

    # TODO: implement
    # (alpha_0, mean_0, cov_0) = init_EM(dimension = dim, nr_components= nr_components, scenario=scenario)
    # ... = EM(x_2dim_pca, nr_components, alpha_0, mean_0, cov_0, max_iter, tol)
    initial_centers = init_k_means(dimension=dim, nr_clusters=nr_components, scenario=scenario, X=x_2dim_pca)
    centers, cum_dist, labels_c = k_means(x_2dim_pca, nr_components, initial_centers, max_iter, tol)

    accuracy = np.sum(labels_c == labels) / len(labels)

    plt.grid()
    plt.scatter(x_2dim_pca[labels_c == 0, 0], x_2dim_pca[labels_c == 0, 1])
    plt.scatter(x_2dim_pca[labels_c == 1, 0], x_2dim_pca[labels_c == 1, 1])
    plt.scatter(x_2dim_pca[labels_c == 2, 0], x_2dim_pca[labels_c == 2, 1])
    plt.scatter(centers[:, 0], centers[:, 1], marker='x', s=150, c='k')
    plt.xlabel('sepal length')
    plt.ylabel('sepal width')
    plt.legend(['label 0', 'label 1', 'label 2', 'means'])
    plt.title('k-means + PCA, accuracy: ' + str(accuracy))
    plt.savefig('kmeans_2dim_pca.pdf')
    plt.show()


# --------------------------------------------------------------------------------
# --------------------------------------------------------------------------------

def init_EM(dimension=2,nr_components=3, scenario=None, X=None):
    """ initializes the EM algorithm
    Input:
        dimension... dimension D of the dataset, scalar
        nr_components...scalar
        scenario... (optional) parameter that allows to further specify the settings, scalar
        X... (optional) samples that may be used for proper inititalization, nr_samples x dimension(D)
    Returns:
        alpha_0... initial weight of each component, 1 x nr_components
        mean_0 ... initial mean values, D x nr_components
        cov_0 ...  initial covariance for each component, D x D x nr_components"""
    # TODO choose suitable initial values for each scenario
    quotient = 1/nr_components
    weight = np.linspace(-0.1, 0.1, nr_components)
    alpha_0 = np.ones([1, nr_components])*quotient
    mean_0 = np.zeros([dimension, nr_components])+weight
    cov_0 = np.ones([dimension, dimension, nr_components])
    if X is not None:
        # for D in range(dimension):
        #     mean_0[D, :] = np.mean(X[:, D]) + weight
        randint = np.random.choice(X.shape[0],nr_components)
        mean_0 = X[randint,:]
        mean_0 = mean_0.T
        print(mean_0.shape)
        for n in range(nr_components):
            cov_0[:, :, n] = np.cov(np.transpose(X))
        # cov_0 = cov_0 + np.random.randn(dimension, dimension, nr_components)
    return alpha_0, mean_0, cov_0

#--------------------------------------------------------------------------------

def EM(X, K, alpha_0, mean_0, cov_0, max_iter, tol, diag=0):
    """ perform the EM-algorithm in order to optimize the parameters of a GMM
    with K components
    Input:
        X... samples, nr_samples x dimension (D)
        K... nr of components, scalar
        alpha_0... initial weight of each component, 1 x K
        mean_0 ... initial mean values, D x K
        cov_0 ...  initial covariance for each component, D x D x K
    Returns:
        alpha... final weight of each component, 1 x K
        mean...  final mean values, D x K
        cov...   final covariance for ech component, D x D x K
        log_likelihood... log-likelihood over all iterations, nr_iterations x 1
        labels... class labels after performing soft classification, nr_samples x 1"""
    nr_samples, dimension = X.shape

    alpha = alpha_0[0]
    mean = mean_0
    cov = cov_0
    if diag == 1:
        for k in range(K):
            cov[:, :, k] = np.diag(np.diag(cov[:, :, k]))

    log_likelihood = np.zeros((max_iter, 1))
    labels = np.zeros((nr_samples, 1))

    for iteration in range(max_iter):
        print(iteration)
        r_nk = np.zeros((nr_samples, K))
        # print(r_nk.shape)
        for k in range(K):
            m_g = alpha[k]*mult_gauss(X, mean[:, k], cov[:, :, k])
            # print(m_g)
            r_nk[:, [k]] = m_g

        r_nk /= np.sum(r_nk, axis=1, keepdims=True)

        Nk = np.sum(r_nk, axis=0)
        alpha = Nk / nr_samples

        for k in range(K):
            mean[:, k] = np.sum(r_nk[:, [k]] * X, axis=0) / Nk[k]
            x_minus_mean = X - mean[:, k]
            cov1 = r_nk[:, [k]]*x_minus_mean
            # print(cov1.shape,x_minus_mean.shape)
            cov[:, :, k] = np.matmul(cov1.T, x_minus_mean) / Nk[k]
            if diag == 1:
                cov[:, :, k] = np.diag(np.diag(cov[:, :, k]))

        # print(cov.shape)
        likelihood = np.zeros((nr_samples,1))
        for k in range(K):
            likelihood += alpha[k]*mult_gauss(X, mean[:, k], cov[:, :, k])
        log_likelihood[iteration] = np.sum(np.log(likelihood))
        # print(log_likelihood)
        
        if iteration > 0 and np.abs(log_likelihood[iteration] - log_likelihood[iteration - 1]) < tol:
            break
        # print(r_nk[1])
    for i in range(nr_samples):
        labels[i] = np.argmax(r_nk[i])
        
        tol = 0.001 * np.abs(log_likelihood[iteration] - log_likelihood[iteration - 1])

    return alpha, mean, cov, log_likelihood[:iteration], labels

def mult_gauss(X, mean, cov):
    nr_samples, dimension = X.shape
    pdf_values = np.zeros((nr_samples, 1))
    inv_cov = np.linalg.inv(cov)
    det_cov = np.sqrt(np.linalg.det(cov))

    for i in range(nr_samples):
        x_minus_mean = X[i, :] - mean
        exponent = -0.5 * np.matmul(np.matmul(x_minus_mean, inv_cov), x_minus_mean.T)
        pdf_values[i] = 1 / ((2 * np.pi) ** (dimension / 2) * det_cov) * np.exp(exponent)

    return pdf_values


# --------------------------------------------------------------------------------

def init_k_means(dimension=None, nr_clusters=None, scenario=None, X=None):
    """ initializes the k_means algorithm
    Input:
        dimension... dimension D of the dataset, scalar
        nr_clusters...scalar
        scenario... (optional) parameter that allows to further specify the settings, scalar
        X... (optional) samples that may be used for proper inititalization, nr_samples x dimension(D)
    Returns:
        initial_centers... initial cluster centers,  D x nr_clusters"""

    if X is None:
        initial_centers = np.random.random((nr_clusters, dimension))
    else:
        initial_centers = np.zeros((nr_clusters, dimension))
        len_X = X.shape[0]
        for cluster in range(nr_clusters):
            i_from = cluster * len_X // nr_clusters
            i_to = (cluster + 1) * len_X // nr_clusters - 1
            initial_centers[cluster, :] = np.mean(X[i_from:i_to], 0)

    return initial_centers


# --------------------------------------------------------------------------------

def k_means(X, K, centers_0, max_iter, tol):
    """ perform the KMeans-algorithm in order to cluster the data into K clusters
    Input:
        X... samples, nr_samples x dimension (D)
        K... nr of clusters, scalar
        centers_0... initial cluster centers,  D x nr_clusters
    Returns:
        centers... final centers, D x nr_clusters
        cumulative_distance... cumulative distance over all iterations, nr_iterations x 1
        labels... class labels after performing hard classification, nr_samples x 1"""
    D = X.shape[1]
    # assert D == centers_0.shape[0]
    # TODO: iteratively update the cluster centers

    cum_dist = [0]
    error = tol + 1
    assigned_label = np.zeros([1, len(X)])
    count = 0
    while error > tol:
        count += 1
        # E STEP
        for samp in range(len(X)):
            dist_vec = np.zeros([1, K])
            for cluster in range(K):
                dist_vec[0, cluster] = np.linalg.norm(X[samp, :] - centers_0[cluster, :])
            assigned_label[0, samp] = np.argmin(dist_vec)

        # M STEP
        curr_dist = 0
        for cluster in range(K):
            assigned_idx = np.where(assigned_label == cluster)[1]
            assigned_samps = X[assigned_idx, :]
            centers_0[cluster, :] = np.mean(assigned_samps, axis=0)

            # Evaluation
            curr_dist = curr_dist + np.linalg.norm(assigned_samps - centers_0[cluster, :])

        cum_dist.append(curr_dist)
        error = abs(cum_dist[-2] - curr_dist)
        if count > max_iter:
            break
    return centers_0, cum_dist, assigned_label.reshape((assigned_label.shape[1],))


# --------------------------------------------------------------------------------

def PCA(data, nr_dimensions=None, whitening=False):
    """ perform PCA and reduce the dimension of the data (D) to nr_dimensions
    Input:
        data... samples, nr_samples x D
        nr_dimensions... dimension after the transformation, scalar
        whitening... False -> standard PCA, True -> PCA with whitening

    Returns:
        transformed data... nr_samples x nr_dimensions
        variance_explained... amount of variance explained by the the first nr_dimensions principal components, scalar"""

    if nr_dimensions is not None:
        dim = nr_dimensions
    else:
        dim = 2
    if whitening:
        data = (data - np.mean(data, axis=0)) / data.std(axis=0)

    cov_mx = np.cov(data, ddof=0, rowvar=False)
    eig_val, eig_vec = np.linalg.eig(cov_mx)
    proj_idx = np.argsort(-eig_val)[0:nr_dimensions]
    proj_vec = eig_vec[:, proj_idx]
    proj_vec = proj_vec.transpose()
    transformed_data = np.matmul(data, proj_vec.transpose())
    variance_explained = sum(eig_val[proj_idx]) / np.sum(eig_val)

    if whitening:
        var_mx = abs(np.diag(1 / eig_val[proj_idx] ** 0.5))
        transformed_data = np.matmul(transformed_data, var_mx)

    return transformed_data, variance_explained


# --------------------------------------------------------------------------------
# --------------------------------------------------------------------------------
# Helper Functions
# --------------------------------------------------------------------------------

def load_iris_data():
    """ loads and modifies the iris data-set
    Input:
    Returns:
        X... samples, 150x4
        Y... labels, 150x1
        feature_names... name of the data columns"""
    iris = datasets.load_iris()
    X = iris.data
    X[50:100, 2] = iris.data[50:100, 2] - 0.25
    Y = iris.target
    return X, Y, iris.feature_names


# --------------------------------------------------------------------------------

def plot_iris_data(data, labels, x_axis, y_axis, title):
    """ plots a 2-dim slice according to the specified labels
    Input:
        data...  samples, 150x2
        labels...labels, 150x1
        x_axis... label for the x_axis
        y_axis... label for the y_axis
        title...  title of the plot"""

    plt.scatter(data[labels == 0, 0], data[labels == 0, 1], label='Iris-Setosa')
    plt.scatter(data[labels == 1, 0], data[labels == 1, 1], label='Iris-Versicolor')
    plt.scatter(data[labels == 2, 0], data[labels == 2, 1], label='Iris-Virgnica')
    plt.xlabel(x_axis)
    plt.ylabel(y_axis)
    plt.title(title)
    plt.legend()
    plt.show()


# --------------------------------------------------------------------------------

def likelihood_multivariate_normal(X, mean, cov, log=False):
    """Returns the likelihood of X for multivariate (d-dimensional) Gaussian
   specified with mu and cov.

   X  ... vector to be evaluated -- np.array([[x_00, x_01,...x_0d], ..., [x_n0, x_n1, ...x_nd]])
   mean ... mean -- [mu_1, mu_2,...,mu_d]
   cov ... covariance matrix -- np.array with (d x d)
   log ... False for likelihood, true for log-likelihood
   """

    dist = multivariate_normal(mean, cov)
    if log is False:
        P = dist.pdf(X)
    elif log is True:
        P = dist.logpdf(X)
    return P


# --------------------------------------------------------------------------------

def plot_gauss_contour(mu, cov, xmin, xmax, ymin, ymax, nr_points, title="Title"):
    """ creates a contour plot for a bivariate gaussian distribution with specified parameters

    Input:
      mu... mean vector, 2x1
      cov...covariance matrix, 2x2
      xmin,xmax... minimum and maximum value for width of plot-area, scalar
      ymin,ymax....minimum and maximum value for height of plot-area, scalar
      nr_points...specifies the resolution along both axis
      title... title of the plot (optional), string"""

    # npts = 100
    delta_x = float(xmax - xmin) / float(nr_points)
    delta_y = float(ymax - ymin) / float(nr_points)
    x = np.arange(xmin, xmax, delta_x)
    y = np.arange(ymin, ymax, delta_y)

    X, Y = np.meshgrid(x, y)
    pos = np.dstack((X, Y))

    Z = multivariate_normal(mu, cov).pdf(pos)
    plt.plot([mu[0]], [mu[1]], 'r+')  # plot the mean as a single point
    CS = plt.contour(X, Y, Z)
    plt.clabel(CS, inline=1, fontsize=10)
    plt.show()
    return


# --------------------------------------------------------------------------------

def sample_discrete_pmf(X, PM, N):
    """Draw N samples for the discrete probability mass function PM that is defined over
    the support X.

    X ... Support of RV -- np.array([...])
    PM ... P(X) -- np.array([...])
    N ... number of samples -- scalar
    """
    assert np.isclose(np.sum(PM), 1.0)
    assert all(0.0 <= p <= 1.0 for p in PM)

    y = np.zeros(N)
    cumulativePM = np.cumsum(PM)  # build CDF based on PMF
    offsetRand = np.random.uniform(0, 1) * (1 / N)  # offset to circumvent numerical issues with cumulativePM
    comb = np.arange(offsetRand, 1 + offsetRand, 1 / N)  # new axis with N values in the range ]0,1[

    j = 0
    for i in range(0, N):
        while comb[i] >= cumulativePM[j]:  # map the linear distributed values comb according to the CDF
            j += 1
        y[i] = X[j]

    return np.random.permutation(y)  # permutation of all samples


# --------------------------------------------------------------------------------

def reassign_class_labels(labels):
    """ reassigns the class labels in order to make the result comparable.
    new_labels contains the labels that can be compared to the provided data,
    i.e., new_labels[i] = j means that i corresponds to j.
    Input:
        labels... estimated labels, 150x1
    Returns:
        new_labels... 3x1"""
    class_assignments = np.array([[np.sum(labels[0:50] == 0), np.sum(labels[0:50] == 1), np.sum(labels[0:50] == 2)],
                                  [np.sum(labels[50:100] == 0), np.sum(labels[50:100] == 1),
                                   np.sum(labels[50:100] == 2)],
                                  [np.sum(labels[100:150] == 0), np.sum(labels[100:150] == 1),
                                   np.sum(labels[100:150] == 2)]])
    new_labels = np.array([np.argmax(class_assignments[:, 0]),
                           np.argmax(class_assignments[:, 1]),
                           np.argmax(class_assignments[:, 2])])
    return new_labels


# --------------------------------------------------------------------------------

def sanity_checks():
    # likelihood_multivariate_normal
    mu = [0.0, 0.0]
    cov = [[1, 0.2], [0.2, 0.5]]
    x = np.array([[0.9, 1.2], [0.8, 0.8], [0.1, 1.0]])
    P = likelihood_multivariate_normal(x, mu, cov)
    print(P)

    plot_gauss_contour(mu, cov, -2, 2, -2, 2, 100, 'Gaussian')

    # sample_discrete_pmf
    PM = np.array([0.2, 0.5, 0.2, 0.1])
    N = 1000
    X = np.array([1, 2, 3, 4])
    Y = sample_discrete_pmf(X, PM, N)

    print('Nr_1:', np.sum(Y == 1),
          'Nr_2:', np.sum(Y == 2),
          'Nr_3:', np.sum(Y == 3),
          'Nr_4:', np.sum(Y == 4))

    # re-assign labels
    class_labels_unordererd = np.array([2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                                        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                                        2, 2, 2, 2, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1,
                                        0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0,
                                        0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0,
                                        0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0])
    new_labels = reassign_class_labels(class_labels_unordererd)
    reshuffled_labels = np.zeros_like(class_labels_unordererd)
    reshuffled_labels[class_labels_unordererd == 0] = new_labels[0]
    reshuffled_labels[class_labels_unordererd == 1] = new_labels[1]
    reshuffled_labels[class_labels_unordererd == 2] = new_labels[2]


# --------------------------------------------------------------------------------
# --------------------------------------------------------------------------------
if __name__ == '__main__':
    sanity_checks()
    main()
