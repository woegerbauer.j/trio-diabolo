# Filename: HW2_LogReg_skeleton.py
# Author: Harald Leisenberger
# Edited: April, 2023

import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import scipy.stats as stats

plt.rcParams['text.usetex'] = True


# --------------------------------------------------------------------------------
# Assignment 2 - Section 2
# --------------------------------------------------------------------------------

def main():
    # !!! All undefined functions should be implemented in the section 'Helper Functions' !!!

    # Load the two data arrays (training set: 400 x 3 - array, test set: 100 x 3 - array)
    # Column 1: feature 1; Column 2: feature 2; Column 3: class label (0 or 1)
    data_training = np.loadtxt('HW2_LogReg_training.data')
    data_test = np.loadtxt('HW2_LogReg_test.data')

    # 2.1 Logistic model fitting -- Gradient descent
    # ----------------------------------------------

    # Fit logistic models with 2D mononomial feature transformations of degree D=1, D=2 and D=3 to the training data.
    # TODO: D = 1,2,3 apply gradient descent to fit the models
    
    # fig,axes = plt.subplots(nrows=3, ncols=1, figsize=(12, 8))
    # eta = 0.5
    # max_iter = 2000
    # epsilon = 10 ** (-3)
    #
    # for D in range(1,4):
    #     def E_tilde(w): return cross_entropy_error(w, data_training, D)
    #
    #     def gradient_E_tilde(w): return gradient_cross_entropy(w, data_training, D)
    #
    #     w0 = np.zeros(sum_1_to_n(D + 1))
    #     w_star, iterations, errors = gradient_descent(E_tilde, gradient_E_tilde, w0, eta, max_iter, epsilon)
    #     print("------")
    #     #axes[D-1,0].figure(figsize=(8, 3), dpi=50, tight_layout=True)
    #     axes[D-1].plot(np.arange(0, iterations+1), errors)
    #     axes[D-1].grid()
    #     #axes[D-1].xlabel('iterations')
    #     #axes[D-1].ylabel('cross entropy error')
    #     #axes[D-1].title('D=3')
    #     #plt.savefig('grad_desc_d3.pdf')
    # plt.show()
    #
    # #eta plots
    # eta_arr = [0.05, 0.5, 1, 5]
    # fig,axes = plt.subplots(nrows=4, ncols=3, figsize=(12, 8))
    # cols = ["D=1", "D=2","D=3"]
    # rows = ["$\eta =$ {}".format(eta) for eta in eta_arr ]
    #
    # for ax, col in zip(axes[0], cols):
    #     ax.set_title(col)
    #
    # for ax, row in zip(axes[:,0], rows):
    #     ax.set_ylabel(row, rotation=90,size="large")
    #
    # max_iter = 2000
    # epsilon = 10 ** (-3)
    # for D in range(1,4):
    #     def E_tilde(w): return cross_entropy_error(w, data_training, D)
    #
    #     def gradient_E_tilde(w): return gradient_cross_entropy(w, data_training, D)
    #
    #     w0 = np.zeros(sum_1_to_n(D + 1))
    #     n=0
    #     for eta in eta_arr:
    #         w_star, iterations, errors = gradient_descent(E_tilde, gradient_E_tilde, w0, eta, max_iter, epsilon)
    #         axes[n,D-1].plot(np.arange(0, iterations+1), errors)
    #         axes[n,D-1].grid()
    #         n=n+1
    #         #axes[D-1].xlabel('iterations')
    #         #axes[D-1].ylabel('cross entropy error')
    #         #axes[D-1].title('D=3')
    #         #plt.savefig('grad_desc_d3.pdf')
    # axes[3,0].set_xlabel("iterations",size="large")
    # axes[3,1].set_xlabel("iterations",size="large")
    # axes[3,2].set_xlabel("iterations",size="large")
    # #plt.savefig('eta_plot_grad.pdf')
    # plt.show()
    #

    
    #
    # # TODO: plot errors (i.e., values of E(w)) against iterations for D = 1,2,3
    #
    # # TODO: Choose different values for the step size eta and discuss the impact on the convergence behavior (D = 1,2,3)
    #
    # # TODO: plot the decision boundaries for your models on both training and test data (D = 1,2,3)
    #
    # # TODO: Compute the percentage of correctly classified points for training and test data (D = 1,2,3)
    #
    # # TODO: fit models for D = 1,2,...,10 to the data and compute the model errors on training and test set
    # eta = 0.5
    #
    # # TODO: plot number of required model parameters against D (find an analytical expression)
    #
    # 2.2 Newton-Raphson algorithm
    # ----------------------------------------------

    # Compare the convergence behavior of the Newton-Raphson algorithm to gradient descent
    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 8))

    eta = 0.5
    D = [1,2,3,4,5,6,7,8,9,10]
    #D=[1,2]
    #D=[3]
    max_iter = 2000
    epsilon = 10**-3

    for d in D:
        def Hessian_E_tilde(w): return Hessian_cross_entropy(w, data_training, d)
        def E_tilde(w): return cross_entropy_error(w, data_training, d)
        def gradient_E_tilde(w): return gradient_cross_entropy(w, data_training, d)

        w0 = np.zeros(sum_1_to_n(d + 1))

        w_star, iterations, errors = gradient_descent(E_tilde, gradient_E_tilde, w0, eta, max_iter, epsilon)
        axes[0].semilogx(np.arange(0, iterations), errors)

        w_star, iterations, errors = Newton_Raphson(E_tilde, gradient_E_tilde, Hessian_E_tilde, w0, eta, max_iter, epsilon)
        axes[1].semilogx(np.arange(0, iterations), errors)
        print(iterations)
    
    

    axes[0].set_xlabel("Iterations",fontsize=20)
    axes[0].set_ylabel("Error",fontsize=20)
    axes[1].legend(D)
    axes[0].set_title("Gradient Descent",fontsize=20)
    axes[1].set_title("Newton Rapshon",fontsize=20)
    axes[1].set_xlabel("Iterations",fontsize=20)
    axes[1].set_ylabel("Error",fontsize=20)
    axes[0].grid()
    axes[1].grid()
    plt.savefig("newton.pdf")
    plt.show()

    pass


# --------------------------------------------------------------------------------
# Helper Functions (to be implemented!)
# --------------------------------------------------------------------------------


def sigmoid(x):
    """ Evaluates value of the sigmoid function for input x.
    
    Input: x ... a real-valued number
    
    Output: sigmoid(x) ... value of sigmoid function (in (0,1))
    """

    # compute sigmoid
    #y = 1 / (1 + np.exp(-x))
    y= 0.5 + np.tanh(x/2)/2
    return y


# --------------------------------------------------------------------------------

def design_matrix_logreg_2D(data, degree):
    """ Creates the design matrix for given data and 2D monomial basis functions as defined in equations ( ) - ( )

    Input: data ... a N x 3 - data array containing N data points (columns 0-1: features; column 2: targets)
           degree ... maximum degree of monomial product between feature 1 and feature 2

    Output: Phi ... the design matrix
    """

    # get number of samples
    N = data.shape[0]

    # calculate number of basis functions / columns of the design matrix
    phi_cols = sum_1_to_n(degree + 1)

    # initialize the design matrix
    Phi = np.zeros((N, phi_cols))

    # iterate through the samples (corresponds to one line of the design matrix)
    for n in range(0, N):

        # fetch corresponding features of n-th sample
        x1 = data[n, 0]
        x2 = data[n, 1]

        # initialize column counter (refers to the columns of the design matrix)
        column = 0

        # helper counter to yield the right order of exponent pairs: (0, 0), (1, 0), (0, 1), (2, 0), (1, 1), ...
        for counter in range(0, degree + 1):

            # d2: exponent that belongs to x2
            for d2 in range(0, counter + 1):
                # calculate design matrix
                Phi[n, column] = x1 ** (counter - d2) * x2 ** d2

                # increase column-counter by one
                column += 1
    return Phi


# --------------------------------------------------------------------------------

def cross_entropy_error(w, data, degree):
    """ Computes the cross-entropy error of a model w.r.t. given data (features + classes)
    
    Input: w ... the model parameter vector
           data ... a N x 3 - data array containing N data points (columns 0-1: features; column 2: targets)
           degree ... maximum degree of monomial product between feature 1 and feature 2
    
    Output: cross_entropy_error ... value of the cross-entropy error function \tilde{E}(w)
    """

    # cross_entropy_error = 0

    # get number of samples
    N = data.shape[0]

    # get targets
    t = data[:, 2]

    # compute design matrix
    phi = design_matrix_logreg_2D(data, degree)

    # compute regression output with w
    y = sigmoid(np.matmul(phi, w))

    # regularization term
    epsilon = 1e-10

    cross_entropy_error = -1/N * (np.inner(t, np.log(y + epsilon)) + np.inner((1 - t), np.log(1 - y + epsilon)))

    # TODO: implement cross entropy error function for 2 features.
    #       You will have to call the function design_matrix_logreg_2D inside this definition.

    # WARNING: If you run into numerical instabilities /overflow during the exercise this could be
    #          due to the usage log(x) with x very close to 0. Hint: replace log(x) with log(x + epsilon)
    #          with epsilon a very small number like or 1e-10.

    return cross_entropy_error


# --------------------------------------------------------------------------------

def gradient_cross_entropy(w, data, degree):
    """ Computes the gradient of the cross-entropy error function w.r.t. a model and given data (features + classes)
    
    Input: w ... the model parameter vector
           data ... a N x 3 - data array containing N data points (columns 0-1: features; column 2: targets)
           degree ... maximum degree of monomial product between feature 1 and feature 2
    
    Output: gradient_cross_entropy ... gradient of the cross-entropy error function \tilde{E}(w)
    """

    gradient_cross_entropy = np.ones((len(w), 1))

    # get number of samples
    N = data.shape[0]

    # get targets
    t = data[:, 2]

    # compute design matrix
    phi = design_matrix_logreg_2D(data, degree)

    # compute regression output with w
    y = sigmoid(np.matmul(phi, w))

    diff_t_y = t - y

    gradient_cross_entropy = -1 / N * np.sum(phi * diff_t_y[:, np.newaxis], axis=0)

    # TODO: implement gradient of the cross entropy error function for 2 features.
    #       You will have to call the function design_matrix_logreg_2D inside this definition.

    return gradient_cross_entropy


# --------------------------------------------------------------------------------

def Hessian_cross_entropy(w, data, degree):
    """ Computes the Hessian of the cross-entropy error function w.r.t. a model and given data (features + classes)
    
    Input: w ... the model parameter vector
           data ... a N x 3 - data array containing N data points (columns 0-1: features; column 2: targets)
           degree ... maximum degree of monomial product between feature 1 and feature 2
    
    Output: Hessian_cross_entropy ... Hesse matrix of the cross-entropy error function \tilde{E}(w)
    """

    Hessian_cross_entropy = np.zeros((len(w), len(w)))

    #compute design matrix
    phi = design_matrix_logreg_2D(data, degree)

    N = data.shape[0]

    y = sigmoid(np.matmul(phi, w))

    for iRow, row in enumerate(phi):
        Hessian_cross_entropy += 1/N*y[iRow]*(1-y[iRow])*np.outer(row, row)


    # TODO: implement Hesse matrix of the cross entropy error function for 2 features.
    #       You will have to call the function design_matrix_logreg_2D inside this definition.

    return Hessian_cross_entropy


# --------------------------------------------------------------------------------

def gradient_descent(fct, grad, w0, eta, max_iter, epsilon):
    """ Performs gradient descent for minimizing an arbitrary function.
    Criterion for convergence: || gradient(w_k) || < epsilon
    
    Input: fct ... the function to be minimized
           grad ... the gradient of the function
           w0 ... starting point for gradient descent
           eta ... step size parameter
           max_iter ... maximum number of iterations to be performed
           epsilon ... tolerance parameter that regulates convergence
    
    Output: w_star ... parameter vector at time of termination
            iterations ... number of iterations performed by gradient descent
            values ... values of the function to be minimized at all iterations
    """

    w_star = w0
    values = np.zeros(max_iter)
    values[0] = fct(w_star)
    gradient = grad(w_star)
    iterations = 1

    # TODO: implement the gradient descent algorithm
    while np.linalg.norm(gradient) > epsilon and iterations < max_iter:
        w_star = w_star - eta*gradient
        values[iterations] = fct(w_star)
        gradient = grad(w_star)
        iterations += 1

    values = values[0:iterations]

    return w_star, iterations, values


# --------------------------------------------------------------------------------

def Newton_Raphson(fct, grad, Hessian, w0, eta, max_iter, epsilon):
    """ Newton-Raphson algorithm for minimizing an arbitrary function.
    Criterion for convergence: || gradient(w_k) || < epsilon
    
    Input: fct ... the function to be minimized
           grad ... the gradient of the function
           Hessian ... the Hesse matrix of the function
           w0 ... starting point for gradient descent
           eta ... step size parameter
           max_iter ... maximum number of iterations to be performed
           epsilon ... tolerance parameter that regulates convergence
    
    Output: w_star ... parameter vector at time of termination
            iterations ... number of iterations performed by gradient descent
            values ... values of the function to be minimized at all iterations
    """

    w_star = w0
    values = np.zeros(max_iter)
    values[0] = fct(w_star)
    gradient = grad(w_star)
    iterations = 1
    relax = 0.05

    while np.linalg.norm(gradient) > epsilon and iterations < max_iter:
        w_star = w_star - eta*np.matmul(np.linalg.inv(Hessian(w_star) + relax*np.eye(len(w_star))), gradient)
        values[iterations] = fct(w_star)
        gradient = grad(w_star)
        iterations += 1

    values = values[0:iterations]

    return w_star, iterations, values


# --------------------------------------------------------------------------------

def sum_1_to_n(n):
    return n * (n + 1) // 2
# --------------------------------------------------------------------------------
# --------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
