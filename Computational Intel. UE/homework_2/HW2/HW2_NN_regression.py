import numpy as np
from sklearn.metrics import mean_squared_error
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
import warnings
import sys

from HW2_NN_regression_plot import plot_mse_vs_neurons, plot_mse_vs_iterations, \
    plot_learned_function, plot_mse_vs_alpha

if not sys.warnoptions:
    import warnings
    warnings.simplefilter("ignore")

"""
Assignment 2: Neural networks
Part 3.1: Regression with neural networks

This file contains functions to train and test the neural networks corresponding the the questions in the assignment,
as mentioned in comments in the functions.
Fill in all the sections containing TODOs.
"""


def calculate_mse(nn, x, y):
    """
    Calculates the mean squared error on the training and test data given the NN model used.
    :param nn: An instance of MLPRegressor or MLPClassifier that has already been trained using fit
    :param x: The data
    :param y: The targets
    :return: Training MSE, Testing MSE
    """
    ## TODO
    mse = 0
    return mse


def ex_3_1_a(x_train, x_test, y_train, y_test):
    """
    Solution for exercise 3.1 a)
    Remember to set alpha to 0 when initializing the model.
    :param x_train: The training dataset
    :param x_test: The testing dataset
    :param y_train: The training targets
    :param y_test: The testing targets
    :return:
    """

    hidden_neurons = [2, 5, 50]

    for nh in hidden_neurons:
        regressor = MLPRegressor(alpha=0, hidden_layer_sizes=(nh,), max_iter=5000)
        pred_train = regressor.fit(x_train, y_train)
        pred_test = regressor.predict(x_test)
        plot_learned_function(nh, x_train, y_train, pred_train, x_test, y_test, pred_test)

def ex_3_1_b(x_train, x_test, y_train, y_test):
    """
    Solution for exercise 3.1 b)
    Remember to set alpha to 0 when initializing the model.
    :param x_train: The training dataset
    :param x_test: The testing dataset
    :param y_train: The training targets
    :param y_test: The testing targets
    :return:
    """

    ## TODO
    pass


def ex_3_1_c(x_train, x_test, y_train, y_test):
    """
    Solution for exercise 3.1 c)
    Remember to set alpha to 0 when initializing the model.
    :param x_train: The training dataset
    :param x_test: The testing dataset
    :param y_train: The training targets
    :param y_test: The testing targets
    :return:
    """

    ## TODO
    pass


def ex_3_1_d(x_train, x_test, y_train, y_test):
    """
    Solution for exercise 3.1 d)
    Remember to set alpha to 0 when initializing the model.
    :param x_train: The training dataset
    :param x_test: The testing dataset
    :param y_train: The training targets
    :param y_test: The testing targets
    :return:
    """

    ## TODO
    pass
