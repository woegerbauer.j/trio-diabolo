import numpy as np
from sklearn.metrics import mean_squared_error
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
import warnings
import sys

from HW2_NN_regression_plot import plot_mse_vs_neurons, plot_mse_vs_iterations, \
    plot_learned_function, plot_mse_vs_alpha

if not sys.warnoptions:
    import warnings
    warnings.simplefilter("ignore")

"""
Assignment 2: Neural networks
Part 3.1: Regression with neural networks

This file contains functions to train and test the neural networks corresponding the the questions in the assignment,
as mentioned in comments in the functions.
Fill in all the sections containing TODOs.
"""


def calculate_mse(nn, x, y):
    """
    Calculates the mean squared error on the training and test data given the NN model used.
    :param nn: An instance of MLPRegressor or MLPClassifier that has already been trained using fit
    :param x: The data
    :param y: The targets
    :return: Training MSE, Testing MSE
    """
    ## TODO
    pred = nn.predict(x)

    mse = np.sum((pred-y)**2)/len(pred)
    return mse



def ex_3_1_a(x_train, x_test, y_train, y_test):
    """
    Solution for exercise 3.1 a)
    Remember to set alpha to 0 when initializing the model.
    :param x_train: The training dataset
    :param x_test: The testing dataset
    :param y_train: The training targets
    :param y_test: The testing targets
    :return:
    """

    hidden_neurons = [2, 5, 50]

    for nh in hidden_neurons:
        regressor = MLPRegressor(alpha=0, hidden_layer_sizes=(nh,), max_iter=5000)
        pred_train = regressor.fit(x_train, y_train)
        pred_test = regressor.predict(x_test)
        plot_learned_function(nh, x_train, y_train, pred_train, x_test, y_test, pred_test)

def ex_3_1_b(x_train, x_test, y_train, y_test):
    """
    Solution for exercise 3.1 b)
    Remember to set alpha to 0 when initializing the model.
    :param x_train: The training dataset
    :param x_test: The testing dataset
    :param y_train: The training targets
    :param y_test: The testing targets
    :return:
    """

    hidden_neurons = 5
    random_states = np.linspace(1, 10, 10)

    #print(x_train)

    for seed in random_states:
        regressor = MLPRegressor(alpha=0, hidden_layer_sizes=(hidden_neurons,), max_iter=5000, random_state=seed)
        pred_train = regressor.fit(x_train, y_train)
    return
def ex_3_1_c(x_train, x_test, y_train, y_test):
    """
    Solution for exercise 3.1 c)
    Remember to set alpha to 0 when initializing the model.
    :param x_train: The training dataset
    :param x_test: The testing dataset
    :param y_train: The training targets
    :param y_test: The testing targets
    :return:
    """
    
    hidden_neurons = np.array([1,2,4,6,8,12,20,40,80,160])
    #seeds = np.array([1,7,19,23,5,60,190,440,230,9])

    seeds = np.abs(np.random.randint(0,1000,size=10))
    #print(seeds)
    
    mse_test = np.zeros((len(hidden_neurons),len(seeds)))
    mse_train = np.zeros((len(hidden_neurons),len(seeds)))

    col=0
    row=0
    for seed in seeds:
        for nh in hidden_neurons:
            regressor = MLPRegressor(alpha=0, hidden_layer_sizes=(nh,), max_iter=5000,random_state = seed)
            trained_model = regressor.fit(x_train, y_train)
            
            #test
            tmp_mse = calculate_mse(trained_model, x_test, y_test)
            mse_test[row,col] = tmp_mse
            
            #train
            tmp_mse = calculate_mse(trained_model, x_train, y_train)
            mse_train[row,col] = tmp_mse
            row=row+1
        col=col+1
        row=0

    plot_mse_vs_neurons(mse_train, mse_test,hidden_neurons)
    
    avg_err_train = np.mean(mse_train,axis=1)
    avg_err_test = np.mean(mse_test,axis=1)
    #print(avg_err_train)
    #print(avg_err_test)
     
    #min_nh_train = np.argmin(avg_err_train)
    #nh = hidden_neurons[min_nh_train]
    
    min_nh_test = np.argmin(avg_err_test)
    nh = hidden_neurons[min_nh_test]
    pred_train = trained_model.predict(x_train)
    pred_test = trained_model.predict(x_test)
    plot_learned_function(nh, x_train, y_train, pred_train, x_test, y_test, pred_test)
    
    return


def ex_3_1_d(x_train, x_test, y_train, y_test):
    """
    Solution for exercise 3.1 d)
    Remember to set alpha to 0 when initializing the model.
    :param x_train: The training dataset
    :param x_test: The testing dataset
    :param y_train: The training targets
    :param y_test: The testing targets
    :return:
    """

    ## TODO
    pass
