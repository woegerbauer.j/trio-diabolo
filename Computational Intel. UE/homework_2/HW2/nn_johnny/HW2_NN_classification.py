from sklearn.metrics import confusion_matrix, mean_squared_error

from sklearn.neural_network import MLPClassifier
from HW2_NN_classification_plot import plot_hidden_layer_weights, plot_boxplot, plot_image
import numpy as np


"""
Assignment 2: Neural networks
Part 3.2: Classification with Neural Networks: Fashion MNIST

This file contains functions to train and test the neural networks corresponding the the questions in the assignment,
as mentioned in comments in the functions.
Fill in all the sections containing TODOs.
"""


def ex_3_2(X_train, y_train, X_test, y_test):
    """
    Snippet for exercise 3.2
    :param X_train: Train set
    :param y_train: Targets for the train set
    :param X_test: Test set
    :param y_test: Targets for the test set
    :return:
    """
    n_hidd = 120
    i = 0
    acc_test = np.zeros(5) 
    acc_train = np.zeros(5)
    acc_best = 0
    seeds = np.random.randint(0,1000,size=5)
    for seed in seeds:
        model = MLPClassifier(hidden_layer_sizes=(n_hidd,), activation='tanh', max_iter=50,random_state=seed)
        trained_model = model.fit(X_train,y_train)
        acc_test[i] = trained_model.score(X_test, y_test)
        #print(acc_test[i])
        acc_train[i] = trained_model.score(X_train, y_train)
        #print(acc_train[i])
        if acc_test[i] > acc_best:
            best_model = trained_model
        i+=1
    trained_model = best_model
    print("Accuracy of test data: {}".format(acc_test))
    print("Accuracy of test data: {}".format(acc_train))
    
        
    #boxplot
    plot_boxplot(acc_train, acc_test)
    
    #hidden layer
    plot_hidden_layer_weights(trained_model.coefs_[0])
    
    #confusion mx
    y_pred = trained_model.predict(X_test)
    conf_mx = confusion_matrix(y_test, y_pred)
    print("Confusion Matrix:")
    print(conf_mx)
    
    #misclassified img
    num_plot = 3
    true_det = np.equal(y_pred,y_test)
    false_det = np.invert(true_det)
    false_det = false_det.nonzero()[0]
    true_det = true_det.nonzero()[0]

    for i in range(num_plot):
        idx = np.random.randint(0,len(false_det)-1)
        plot_image(X_test[idx])
    
        
    pass
