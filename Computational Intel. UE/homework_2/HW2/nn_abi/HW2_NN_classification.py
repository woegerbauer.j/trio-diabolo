from sklearn.metrics import confusion_matrix, mean_squared_error

from sklearn.neural_network import MLPClassifier
from HW2_NN_classification_plot import plot_hidden_layer_weights, plot_boxplot, plot_image
import numpy as np


"""
Assignment 2: Neural networks
Part 3.2: Classification with Neural Networks: Fashion MNIST

This file contains functions to train and test the neural networks corresponding the the questions in the assignment,
as mentioned in comments in the functions.
Fill in all the sections containing TODOs.
"""


def ex_3_2(X_train, y_train, X_test, y_test):
    """
    Snippet for exercise 3.2
    :param X_train: Train set
    :param y_train: Targets for the train set
    :param X_test: Test set
    :param y_test: Targets for the test set
    :return:
    """
    
    ## TODO
    pass
