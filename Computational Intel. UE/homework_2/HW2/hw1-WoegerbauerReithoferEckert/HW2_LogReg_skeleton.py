#Filename: HW2_LogReg_skeleton.py
#Author: Harald Leisenberger
#Edited: April, 2023

import numpy as np
import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import scipy.stats as stats
import time

plt.rcParams.update({
    "text.usetex": True,
    "font.family": "Helvetica"
})

#--------------------------------------------------------------------------------
# Assignment 2 - Section 2
#--------------------------------------------------------------------------------

def main():    
    

    # !!! All undefined functions should be implemented in the section 'Helper Functions' !!!
    
    start = time.time()
    # Load the two data arrays (training set: 400 x 3 - array, test set: 100 x 3 - array)
    # Column 1: feature 1; Column 2: feature 2; Column 3: class label (0 or 1)
    data_training = np.loadtxt('HW2_LogReg_training.data')
    data_test = np.loadtxt('HW2_LogReg_test.data')
    
    
    # 2.1 Logistic model fitting -- Gradient descent
    # ----------------------------------------------
    
    # Fit logistic models with 2D mononomial feature transformations of degree D=1, D=2 and D=3 to the training data.
    # TODO: D = 1,2,3 apply gradient descent to fit the models
    c1='yellow' #yellow
    c2='green' #green
    c3='red'
    c4='blue'
    ncolor=500
    
    deg = np.arange(1,11)
    etas = [0.05,0.5,1,5]
    count = 0
    max_iter = 2000
    epsilon = 10**(-3)
    Ergebnisse = [['train/test','degree','eta','w_star','iterations','erros','w_star_list']]
    Ergebnisse_Hessian = [['train/test','degree','eta','w_star','iterations','erros','w_star_list']]
    def E_tilde(w): return cross_entropy_error(w,data_training,D)
    def gradient_E_tilde(w): return gradient_cross_entropy(w,data_training,D)
    for D in deg:
        for eta in etas:
            count += 1
            
            w0 = np.zeros(int((D+1)*(D+2)/2))
            w = w0
            
            # TODO: plot errors (i.e., values of E(w)) against iterations for D = 1,2,3
            if eta == 0.5:
                w_star, iterations, errors, w_star_list = gradient_descent(E_tilde,gradient_E_tilde,w0,eta,max_iter,epsilon)
                listinger = ['training',D,eta,w_star,iterations,errors,w_star_list]
                Ergebnisse.append(listinger)
                
                print(D,'training',iterations,errors[iterations-1])
                
                errors = np.array([])
                # print(w_star_list)
                for i in range(iterations):
                    w_star = w_star_list[i,:]
                    errors = np.append(errors,cross_entropy_error(w_star, data_test, D))
                
                listinger = ['test',D,eta,w_star,iterations,errors,w_star_list]
                Ergebnisse.append(listinger)
                
                print(D,'test',iterations,errors[iterations-1])
            
            if D <= 3 and eta != 0.5:
                w_star, iterations, errors, w_star_list = gradient_descent(E_tilde,gradient_E_tilde,w0,eta,max_iter,epsilon)
                listinger = ['training',D,eta,w_star,iterations,errors,w_star_list]
                Ergebnisse.append(listinger)
    
    print(time.time()-start)
    
    fig1 = plt.figure(figsize=(12, 8))
    spall = fig1.add_subplot(1,1,1)
    
    spall.spines['top'].set_color('none')
    spall.spines['bottom'].set_color('none')
    spall.spines['left'].set_color('none')
    spall.spines['right'].set_color('none')
    spall.tick_params(labelcolor='w', top=False, bottom=False, left=False, right=False)
    # TODO: Choose different values for the step size eta and discuss the impact on the convergence behavior (D = 1,2,3)
    # TODO: fit models for D = 1,2,...,10 to the data and compute the model errors on training and test set
    fig2 = plt.figure(figsize=(12, 8))
    ax2 = fig2.add_subplot(1,1,1)
    ax2.spines['top'].set_color('none')
    ax2.spines['bottom'].set_color('none')
    ax2.spines['left'].set_color('none')
    ax2.spines['right'].set_color('none')
    ax2.tick_params(labelcolor='w', top=False, bottom=False, left=False, right=False)
    sp2 = fig2.add_subplot(1,2,1)
    sp3 = fig2.add_subplot(1,2,2)
    
    count = 0
    
    for i in range(1,len(Ergebnisse)):       
        # print(i)
        iterations = Ergebnisse[i][4]
        errors = Ergebnisse[i][5]
        D = Ergebnisse[i][1]
        eta = Ergebnisse[i][2]
            
        if eta == 0.5:
            if Ergebnisse[i][0] == 'training':
                sp2.plot(range(iterations),errors,label='D = '+ str(D), color=colorFader(c1, c2, D/len(deg)))
            if Ergebnisse[i][0] == 'test':
                sp3.plot(range(iterations),errors,label='D = '+ str(D), color=colorFader(c1, c2, D/len(deg)))
        if D <= 3 and Ergebnisse[i][0] == 'training':
            count += 1
            # print(D,eta,count)
            sp = fig1.add_subplot(3,4,count)
            sp.plot(range(iterations),errors,label='D = '+str(D)+' & '+'$\eta$ = '+str(eta), color=colorFader(c1, c2, count/12))
            sp.grid()
            
            if count >= 9:
                sp.set_xlabel('$\eta$ = ' + str(eta))
            if count == 1 or count == 5 or count == 9:
                sp.set_ylabel('D = ' + str(D))
                
    spall.set_xlabel('Number of Iterations', labelpad = 20)
    spall.set_ylabel('Error', labelpad = 30)
    sp3.legend()
    sp2.set_xscale('log')
    sp3.set_xscale('log')
    sp2.set_title('Training Data')
    sp3.set_title('Test Data')
    
    ax2.set_xlabel('Number of Iterations', labelpad = 20)
    ax2.set_ylabel('Error', labelpad = 30)
    
    # TODO: plot the decision boundaries for your models on both training and test data (D = 1,2,3)
    min_x1 = min(data_training.T[0])
    min_x2 = min(data_training.T[1])
    max_x1 = max(data_training.T[0])
    max_x2 = max(data_training.T[1])
    x1_vec = np.arange(min_x1-1,max_x1+1,0.01)
    x2_vec = np.arange(min_x2-1,max_x2+1,0.01)

    x1_vec, x2_vec = np.meshgrid(x1_vec,x2_vec)

    fi,axes = plt.subplots(nrows=3,ncols=2,figsize=(12,10))
    for D in range(1,4):

        def E_tilde(w): return cross_entropy_error(w, data_training, D)

        def gradient_E_tilde(w): return gradient_cross_entropy(w, data_training, D)

        w0 = np.zeros(int((D+1)*(D+2)/2))
        w_star, iterations, errors, w_star_list = gradient_descent(E_tilde, gradient_E_tilde, w0, eta, max_iter, epsilon)

        pred_out=sigmoid(design_matrix_logreg_2D(np.array([x1_vec.ravel(),x2_vec.ravel()]).T, D).dot(w_star))
        pred_out = pred_out.reshape(x1_vec.shape)

        axes[D-1,0].contourf(x1_vec,x2_vec,pred_out)
        axes[D-1,1].contourf(x1_vec,x2_vec,pred_out)


        c = ["y" if samp == 1 else "b" for samp in data_training.T[2]]
        axes[D-1,0].scatter(data_training.T[0],data_training.T[1],c=c,s=6)

        c = ["y" if samp == 1 else "b" for samp in data_test.T[2]]
        axes[D-1,1].scatter(data_test.T[0],data_test.T[1],c=c,s=6)
        axes[D-1,0].set_ylabel("D = {}".format(D),size="large")

    axes[2,0].set_xlabel("Trainings Data",size="large")
    axes[2,1].set_xlabel("Test Data",size="large")
    plt.show()
    
    # fig_dec = plt.figure(figsize=(12, 8))
    # n = 0
    # for d in range(3):
    #     n += 1
    #     D1_train = fig_dec.add_subplot(3,2,n)
    #     targ = data_training[:,2]
    #     idx1 = []
    #     idx0 = []
    #     for i, value in enumerate(targ):
    #         if value == 1:
    #             idx1.append(i)
    #         else:
    #             idx0.append(i)
    #     D1_train.scatter(data_training[idx1,0],data_training[idx1,1], c = '#0000FF')
    #     D1_train.scatter(data_training[idx0,0],data_training[idx0,1], c = '#FFFF00')
        
    #     D1_test = fig_dec.add_subplot(3,2,n+3)
    #     targ = data_test[:,2]
    #     idx1 = []
    #     idx0 = []
    #     for i, value in enumerate(targ):
    #         if value == 1:
    #             idx1.append(i)
    #         else:
    #             idx0.append(i)
    #     D1_test.scatter(data_test[idx1,0],data_test[idx1,1], c = '#0000FF')
    #     D1_test.scatter(data_test[idx0,0],data_test[idx0,1], c = '#FFFF00')
    # D1_train.fill_between()
    
    
    # TODO: Compute the percentage of correctly classified points for training and test data (D = 1,2,3)
    
    
    eta = 0.5
    
    # TODO: plot number of required model parameters against D (find an analytical expression)
    
    
    # 2.2 Newton-Raphson algorithm
    # ----------------------------------------------
    
    # Compare the convergence behavior of the Newton-Raphson algorithm to gradient descent
    for D in deg:
        w0 = np.zeros((int((D+1)*(D+2)/2)))
        w = w0
        landa = 0.05
        def E_tilde(w): return cross_entropy_error(w,data_training,D)
        def gradient_E_tilde(w): return gradient_cross_entropy(w,data_training,D)
        def Hessian_E_tilde(w): return Hessian_johnninger_joe(w,data_training,D,landa)
        
        w_star, iterations, errors, w_star_list = Newton_Raphson(E_tilde,gradient_E_tilde,Hessian_E_tilde,w0,eta,max_iter,epsilon)
        listinger = ['training_j',D,eta,w_star,iterations,errors,w_star_list]
        Ergebnisse_Hessian.append(listinger)
        
        print(D,'training_j',iterations,errors[iterations-1])
        
        errors = np.array([])
        # print(w_star_list)
        for i in range(iterations):
            w_star = w_star_list[i,:]
            errors = np.append(errors,cross_entropy_error(w_star, data_test, D))
        
        listinger = ['test_j',D,eta,w_star,iterations,errors,w_star_list]
        Ergebnisse_Hessian.append(listinger)
        
        print(D,'test_j',iterations,errors[iterations-1])
    
    
    deg = np.arange(1,7)
    eta = 0.5
    for D in deg:
        w0 = np.zeros((int((D+1)*(D+2)/2)))
        w = w0
        
        def E_tilde(w): return cross_entropy_error(w,data_training,D)
        def gradient_E_tilde(w): return gradient_cross_entropy(w,data_training,D)
        def Hessian_E_tilde(w): return Hessian_cross_entropy(w,data_training,D)
        
        w_star, iterations, errors, w_star_list = Newton_Raphson(E_tilde,gradient_E_tilde,Hessian_E_tilde,w0,eta,max_iter,epsilon)
        listinger = ['training',D,eta,w_star,iterations,errors,w_star_list]
        Ergebnisse_Hessian.append(listinger)
        
        print(D,'training',iterations,errors[iterations-1])
        
        errors = np.array([])
        # print(w_star_list)
        for i in range(iterations):
            w_star = w_star_list[i,:]
            errors = np.append(errors,cross_entropy_error(w_star, data_test, D))
        
        listinger = ['test',D,eta,w_star,iterations,errors,w_star_list]
        Ergebnisse_Hessian.append(listinger)
        
        print(D,'test',iterations,errors[iterations-1])
        pass
    
    fig_hess = plt.figure(figsize=(12, 8))
    hess = fig_hess.add_subplot(1,1,1)
    
    hess.spines['top'].set_color('none')
    hess.spines['bottom'].set_color('none')
    hess.spines['left'].set_color('none')
    hess.spines['right'].set_color('none')
    hess.tick_params(labelcolor='w', top=False, bottom=False, left=False, right=False)
    
    plt1 = fig_hess.add_subplot(1,2,1)
    plt2 = fig_hess.add_subplot(1,2,2)
    for i in range(1,len(Ergebnisse)):       
        # print(i)
        iterations = Ergebnisse[i][4]
        errors = Ergebnisse[i][5]
        D = Ergebnisse[i][1]
        eta = Ergebnisse[i][2]
        
        if eta == 0.5 and Ergebnisse[i][0] == 'training':
            plt1.plot(range(iterations),errors,label='D = '+ str(D), color=colorFader(c1, c2, D/10))
    for i in range(1,len(Ergebnisse_Hessian)):
        iterations = Ergebnisse_Hessian[i][4]
        errors = Ergebnisse_Hessian[i][5]
        D = Ergebnisse_Hessian[i][1]
        eta = Ergebnisse_Hessian[i][2]
            
        if eta == 0.5 and Ergebnisse_Hessian[i][0] == 'training':
            plt2.plot(range(iterations),errors,label='D = '+ str(D), color=colorFader(c1, c2, D/len(deg)))
        if eta == 0.5 and Ergebnisse_Hessian[i][0] == 'training_j':
            plt2.plot(range(iterations),errors,label='D = '+ str(D) + ', $\lambda = $' + str(landa), color=colorFader(c3, c4, D/10), linestyle = '-.')
    # plt1.legend()
    plt1.grid()
    plt2.grid()
    plt2.legend()
    plt1.legend()
    # plt1.set_yscale('log')
    # plt2.set_yscale('log')
    plt1.set_xscale('log')
    plt2.set_xscale('log')
    plt1.set_title('Gradient Descent')
    plt2.set_title('Newton-Raphson')
    hess.set_xlabel('Number of Iterations', labelpad = 10)
    hess.set_ylabel('Error', labelpad = 10)
    
    return Ergebnisse, Ergebnisse_Hessian
    
    
#--------------------------------------------------------------------------------
# Helper Functions (to be implemented!)
#--------------------------------------------------------------------------------
def colorFader(c1,c2,mix=0): #fade (linear interpolate) from color c1 (at mix=0) to c2 (mix=1)
    c1=np.array(mpl.colors.to_rgb(c1))
    c2=np.array(mpl.colors.to_rgb(c2))
    return mpl.colors.to_hex((1-mix)*c1 + mix*c2)
    
def sigmoid(x):
    
    """ Evaluates value of the sigmoid function for input x.
    
    Input: x ... a real-valued number
    
    Output: sigmoid(x) ... value of sigmoid function (in (0,1))
    """
    
    # TODO: implement sigmoid function
    
    # y = 1/(1+np.exp(-x))
    
    y = 0.5 + np.tanh(x/2)/2
    
    return y

#--------------------------------------------------------------------------------

def design_matrix_logreg_2D(data,degree):
    
    """ Creates the design matrix for given data and 2D monomial basis functions as defined in equations ( ) - ( )
    
    Input: data ... a N x 3 - data array containing N data points (columns 0-1: features; column 2: targets)
           degree ... maximum degree of monomial product between feature 1 and feature 2
           
    Output: Phi ... the design matrix
    """
    
    # TODO: compute the design matrix for 2 features and 2D monomial basis functions
    data_len = len(data)
    Phi = np.zeros((data_len,int((degree+1)*(degree+2)/2)))   
    count = 0
    for i in range(degree+1):
        for j in range(i+1):
            Phi[:,count] = np.multiply(data[:,0]**(i-j),data[:,1]**j)
            count += 1
            
    
    return Phi

#--------------------------------------------------------------------------------

def cross_entropy_error(w,data,degree):
    
    """ Computes the cross-entropy error of a model w.r.t. given data (features + classes)
    
    Input: w ... the model parameter vector
           data ... a N x 3 - data array containing N data points (columns 0-1: features; column 2: targets)
           degree ... maximum degree of monomial product between feature 1 and feature 2
    
    Output: cross_entropy_error ... value of the cross-entropy error function \tilde{E}(w)
    """
    
    cross_entropy_error = 0
    
    # TODO: implement cross entropy error function for 2 features.
    #       You will have to call the function design_matrix_logreg_2D inside this definition.
    N = data.shape[0]
    x1, x2, t_n = data[:,0], data[:,1], data[:,2]
    Phi = design_matrix_logreg_2D(data, degree)
    # print(w)
    y_n = sigmoid(np.matmul(Phi,w))
    epsilon = 1e-10
    
    cross_entropy_error = -1/N*(np.matmul(t_n, np.log(y_n + epsilon)) + np.matmul((1-t_n),np.log(1 - y_n + epsilon)))
    
    
    # WARNING: If you run into numerical instabilities /overflow during the exercise this could be
    #          due to the usage log(x) with x very close to 0. Hint: replace log(x) with log(x + epsilon)
    #          with epsilon a very small number like or 1e-10.
    
    return cross_entropy_error

#--------------------------------------------------------------------------------

def gradient_cross_entropy(w,data,degree):
    
    """ Computes the gradient of the cross-entropy error function w.r.t. a model and given data (features + classes)
    
    Input: w ... the model parameter vector
           data ... a N x 3 - data array containing N data points (columns 0-1: features; column 2: targets)
           degree ... maximum degree of monomial product between feature 1 and feature 2
    
    Output: gradient_cross_entropy ... gradient of the cross-entropy error function \tilde{E}(w)
    """
    
    gradient_cross_entropy = np.zeros((len(w),1))
    
    # TODO: implement gradient of the cross entropy error function for 2 features.
    #       You will have to call the function design_matrix_logreg_2D inside this definition.
    N = data.shape[0]
    
    x1, x2, t_n = data[:,0], data[:,1], data[:,2]
    Phi = design_matrix_logreg_2D(data, degree)
    # w = np.array(w, dtype = float128)
    y_n = sigmoid(np.matmul(Phi,w))
    gradient_cross_entropy = -1/N*np.matmul((t_n - y_n),Phi)
    
    

    return gradient_cross_entropy

#--------------------------------------------------------------------------------

def Hessian_cross_entropy(w,data,degree):
    
    """ Computes the Hessian of the cross-entropy error function w.r.t. a model and given data (features + classes)
    
    Input: w ... the model parameter vector
           data ... a N x 3 - data array containing N data points (columns 0-1: features; column 2: targets)
           degree ... maximum degree of monomial product between feature 1 and feature 2
    
    Output: Hessian_cross_entropy ... Hesse matrix of the cross-entropy error function \tilde{E}(w)
    """
    
    Hessian_cross_entropy = np.zeros((len(w),len(w)))
    
    # TODO: implement Hesse matrix of the cross entropy error function for 2 features.
    #       You will have to call the function design_matrix_logreg_2D inside this definition.
    N = data.shape[0]
    Phi = design_matrix_logreg_2D(data, degree)
    y_n = sigmoid(np.matmul(Phi,w))
    for n in range(len(y_n)):
        Hessian_cross_entropy += y_n[n]*(1-y_n[n])*np.outer(Phi[n,:],Phi[n,:])
    Hessian_cross_entropy = Hessian_cross_entropy/N
    # for iRow, row in enumerate(Phi):
    #     Hessian_cross_entropy += 1/N*y_n[iRow]*(1-y_n[iRow])*np.outer(row, row)
        
    
    # print(Hessian_cross_entropy)
    
    return Hessian_cross_entropy

def Hessian_johnninger_joe(w,data,degree,landa):
    
    """ Computes the Hessian of the cross-entropy error function w.r.t. a model and given data (features + classes)
    
    Input: w ... the model parameter vector
           data ... a N x 3 - data array containing N data points (columns 0-1: features; column 2: targets)
           degree ... maximum degree of monomial product between feature 1 and feature 2
    
    Output: Hessian_cross_entropy ... Hesse matrix of the cross-entropy error function \tilde{E}(w)
    """
    
    Hessian_cross_entropy = np.zeros((len(w),len(w)))
    
    # TODO: implement Hesse matrix of the cross entropy error function for 2 features.
    #       You will have to call the function design_matrix_logreg_2D inside this definition.
    N = data.shape[0]
    Phi = design_matrix_logreg_2D(data, degree)
    y_n = sigmoid(np.matmul(Phi,w))
    for n in range(len(y_n)):
        Hessian_cross_entropy += y_n[n]*(1-y_n[n])*np.outer(Phi[n,:],Phi[n,:])
    Hessian_cross_entropy = Hessian_cross_entropy/N + landa*np.eye(Hessian_cross_entropy.shape[0])
    # for iRow, row in enumerate(Phi):
    #     Hessian_cross_entropy += 1/N*y_n[iRow]*(1-y_n[iRow])*np.outer(row, row)
        
    
    # print(Hessian_cross_entropy)
    
    return Hessian_cross_entropy

#--------------------------------------------------------------------------------

def gradient_descent(fct,grad,w0,eta,max_iter,epsilon):
    
    """ Performs gradient descent for minimizing an arbitrary function.
    Criterion for convergence: || gradient(w_k) || < epsilon
    
    Input: fct ... the function to be minimized
           grad ... the gradient of the function
           w0 ... starting point for gradient descent
           eta ... step size parameter
           max_iter ... maximum number of iterations to be performed
           epsilon ... tolerance parameter that regulates convergence
    
    Output: w_star ... parameter vector at time of termination
            iterations ... number of iterations performed by gradient descent
            values ... values of the function to be minimized at all iterations
    """
    
    w_star = w0
    iterations = 0
    values = np.array([])
    w_star_list = [w0]
    
    # TODO: implement the gradient descent algorithm
    while np.linalg.norm(grad(w_star)) >= epsilon and iterations < max_iter:
        w_star = w_star - np.multiply(eta,grad(w_star))
        values = np.append(values,fct(w_star))
        w_star_list = np.append(w_star_list,[w_star], axis = 0)
        iterations += 1
        
    
    
    return w_star, iterations, values, w_star_list

#--------------------------------------------------------------------------------

def Newton_Raphson(fct,grad,Hessian,w0,eta,max_iter,epsilon):
    
    """ Newton-Raphson algorithm for minimizing an arbitrary function.
    Criterion for convergence: || gradient(w_k) || < epsilon
    
    Input: fct ... the function to be minimized
           grad ... the gradient of the function
           Hessian ... the Hesse matrix of the function
           w0 ... starting point for gradient descent
           eta ... step size parameter
           max_iter ... maximum number of iterations to be performed
           epsilon ... tolerance parameter that regulates convergence
    
    Output: w_star ... parameter vector at time of termination
            iterations ... number of iterations performed by gradient descent
            values ... values of the function to be minimized at all iterations
    """
    
    w_star = w0
    iterations = 0
    values = np.array([])
    w_star_list = [w0]
    
    # TODO: implement the Newton-Raphson algorithm
    
    while np.linalg.norm(grad(w_star)) >= epsilon and iterations < max_iter:
        w_star = w_star - eta*np.matmul(np.linalg.inv(Hessian(w_star)),grad(w_star))
        values = np.append(values,fct(w_star))
        w_star_list = np.append(w_star_list,[w_star], axis = 0)
        iterations += 1
        
        
    
    return w_star, iterations, values, w_star_list

def sum_1_to_n(n):
    return n * (n + 1) // 2
#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
