#Filename: HW1_MLE_skeleton.py
#Author: Harald Leisenberger
#Edited: March, 2023

import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import scipy.stats as stats

#--------------------------------------------------------------------------------
# Assignment 1 - Section 2
#--------------------------------------------------------------------------------

def main():    
    
    
    
    # Load the three data arrays (100 x 1 - arrays)
    data1 = np.loadtxt('HW1_MLE_1.data')
    data2 = np.loadtxt('HW1_MLE_2.data')
    data3 = np.loadtxt('HW1_MLE_3.data')
    
    
    # 2.1 Maximum Likelihood Model Estimation (MLE)
    # ---------------------------------------------
    
    # histogram plots of the data
    fig1 = plt.figure("Figure 2")
    plt.hist(data1,bins="auto")
    plt.title("Histogram of data 1")
    plt.xlabel("data")
    plt.ylabel("frequency")
    plt.grid()
    fig1.savefig('samplefigure', bbox_inches='tight')
    
    plt.show()
    
    fig2 = plt.figure("Figure 2")
    plt.hist(data2,bins="auto")
    plt.title("Histogram of data 2")
    plt.xlabel("data")
    plt.ylabel("frequency")
    plt.grid()
    plt.show()
    
    fig3 = plt.figure("Figure 2")
    plt.hist(data3,bins="auto")
    plt.title("Histogram of data 3")
    plt.xlabel("data")
    plt.ylabel("frequency")
    plt.grid()
    plt.show()
    

    # Estimate the true model parameters via MLE
    ML_param_1 = ML_estimation(data1)
    ML_param_2 = ML_estimation(data2)
    ML_param_3 = ML_estimation(data3)
    
    print(f"Data 1 : {ML_param_1}\nData 2: {ML_param_2}\nData 3: {ML_param_3}\n")
    
    fig4 = plt.figure("Figure 2")
    x_axis = np.arange(min(data1),max(data1),0.01)
    plt.plot(x_axis,stats.norm.pdf(x_axis,ML_param_1[0],np.sqrt(ML_param_1[1])))
    plt.hist(data1,density=True,bins="auto")
    plt.xlabel("data")
    plt.ylabel("PDF/frequency")
    plt.grid()
    
    plt.show()
    
    fig5 = plt.figure("Figure 2")
    x_axis = np.arange(min(data2),max(data2),0.01)
    plt.plot(x_axis,stats.norm.pdf(x_axis,ML_param_2[0],np.sqrt(ML_param_2[1])))
    plt.hist(data2,density=True,bins="auto")
    plt.xlabel("data")
    plt.ylabel("PDF/frequency")
    plt.grid()
    plt.show()
    
    fig6 = plt.figure("Figure 2")
    x_axis = np.arange(min(data3),max(data3),0.01)
    plt.plot(x_axis,ML_param_3[0]*np.exp(-1*ML_param_3[0]*x_axis))
    plt.hist(data3,density=True,bins="auto")
    plt.xlabel("data")
    plt.ylabel("PDF/frequency")    
    plt.grid()
    plt.show()
    
    
    
    datai = data1
    dataj = data2
    datak = data3
    


    
    #exp values
    lambda_min=0.1
    lambda_max=4
    resolution_lambda = 0.0001
    
    # 2.2 Evaluation and Visualization of the Likelihood Function
    # -----------------------------------------------------------
    
    # 3D plots of the joint likelihood functions of the Gaussian distributed data

    mu_min = 2.85
    mu_max = 3.08
    sigma_sq_min = 0.011
    sigma_sq_max = 0.17
    resolution_mu = 0.0001
    resolution_sigma_sq = 0.00001
    
    
    plot_likelihood_Gauss(datai,mu_min,mu_max,sigma_sq_min,sigma_sq_max,resolution_mu,resolution_sigma_sq)
    ML_num_i = ML_numerical_Gauss(datai,mu_min,mu_max,sigma_sq_min,sigma_sq_max,resolution_mu,resolution_sigma_sq)
    
    
    
    mu_min = -2.05
    mu_max = -1.95
    sigma_sq_min = 1.1
    sigma_sq_max = 1.27
    resolution_mu = 0.0001
    resolution_sigma_sq = 0.0001
    plot_likelihood_Gauss(dataj,mu_min,mu_max,sigma_sq_min,sigma_sq_max,resolution_mu,resolution_sigma_sq) 
    ML_num_j = ML_numerical_Gauss(dataj,mu_min,mu_max,sigma_sq_min,sigma_sq_max,resolution_mu,resolution_sigma_sq)
    
    # 2D plot of the joint likelihood function of exponential distributed data
    plot_likelihood_Exp(datak,lambda_min,lambda_max,resolution_lambda) 

    # Numerical MLE for the exponential distributed data
    ML_num_k = ML_numerical_Exp(datak,lambda_min,lambda_max,resolution_lambda)
    
    
    # 2.3 Bayesian Model Estimation
    # -----------------------------
    
    
    mu_prior = 0.5
    sigma_sq_prior = 1
    
    
    mu_ML = ML_param_1[0]
    sigma_sq_true = ML_param_1[1]
    posterior_param_i = posterior_mean_Gauss(datai,mu_prior,mu_ML,sigma_sq_prior,sigma_sq_true)
    
    mu_ML = ML_param_2[0]
    sigma_sq_true = ML_param_2[1]
    posterior_param_j = posterior_mean_Gauss(dataj,mu_prior,mu_ML,sigma_sq_prior,sigma_sq_true)
    
    fig7 = plt.figure("Figure 2")
    
    x_axis_1 = np.arange(min(data1),max(data1),0.01)
    x_axis_2 = np.arange(min(data2),max(data2),0.01)
    x_axis_prior = np.arange(min(np.append(data1,data2)),max(np.append(data1,data2)),0.01)
    
    plt.plot(x_axis_1,stats.norm.pdf(x_axis_1,posterior_param_i[0],np.sqrt(posterior_param_i[1])))
    
    plt.plot(x_axis_2,stats.norm.pdf(x_axis_2,posterior_param_j[0],np.sqrt(posterior_param_j[1])))
    
    plt.plot(x_axis_prior,stats.norm.pdf(x_axis_prior,mu_prior,np.sqrt(sigma_sq_prior)))
    plt.legend(["Posterior for Data 1","Posterior for Data 2","Prior Distribution"])
    plt.xlabel("values for mu")
    plt.ylabel("PDF")
    plt.grid()
    plt.show()
    
    
    
    pass


#--------------------------------------------------------------------------------
# Helper Functions (to be implemented!)
#--------------------------------------------------------------------------------


def ML_estimation(data):
    
    """ estimates the maximum likelihood parameters for a given data sample.
    
    Input:  data ... an array of 1-dimensional data points
    
    Output: ML_param ... the values of the maximum likelihood estimators for all parameters
                         of the corresponding distribution (i.e., 1-D Gaussian or 1-D exponential)
    """
    
    tolerance = 1
    data_d = np.digitize(data, np.linspace(min(data), max(data), num=10))
    data_len = len(data)
    
    if abs(np.mean(data_d) - 5) < tolerance:
        #GAUSS
        print("Estimated to be Gaussian!")
        mu_tilde = 1/data_len * np.sum(data)
        var_tilde = 1/data_len * np.sum((data-mu_tilde)**2)
        ML_param = [mu_tilde,var_tilde]
    
    else:
        #EXPON
        print("Estimated to be exponential!")
        lambda_tilde = data_len / (np.sum(data))
        ML_param = [lambda_tilde]
        
    return ML_param
                                                                                                                  


#--------------------------------------------------------------------------------

def plot_likelihood_Gauss(data,mu_min,mu_max,sigma_sq_min,sigma_sq_max,resolution_mu,resolution_sigma_sq):
    
    """ Plots the joint likelihood function for mu and sigma^2 for a given 1-D Gaussian data sample on
        a predefined grid.
    
    Input:  data ... an array of 1-dimensional Gaussian distributed data points
            mu_min ... lower boundary of the grid on the mu-axis
            mu_max ... upper boundary of the grid on the mu-axis
            sigma_sq_min ... lower boundary of the grid on the sigma^2-axis
            sigma_sq_max ... upper boundary of the grid on the sigma^2-axis
            resolution_mu ... interval length between discretized points on the mu-axis
            
            resolution_sigma_sq ... interval length between discretized points on the sigma^2-axis
    Output: ---
    """
    



    mu_arr = np.arange(mu_min,mu_max,resolution_mu)
    var_arr = np.arange(sigma_sq_min,sigma_sq_max,resolution_sigma_sq)
    mu_array,var_array = np.meshgrid(mu_arr,var_arr)

    data_len = len(data)

    sum_array = []
    for tmp_mu in mu_array[1,0:]:
        sum_array.append(np.sum((data-tmp_mu)**2))
    sum_arr=np.tile(sum_array,(len(var_arr),1))

    ML_val = data_len * np.log(1) - data_len / 2 * np.log(2 * np.pi * var_array) - 1 / 2 * np.divide(sum_arr,var_array)



    ax = plt.figure().add_subplot(projection='3d')

    surf = ax.plot_surface(mu_array, var_array, ML_val, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)

    ax.xaxis.set_rotate_label(False)
    ax.yaxis.set_rotate_label(False)
    ax.zaxis.set_rotate_label(False)

    ax.xaxis.labelpad=6
    ax.yaxis.labelpad=4
    ax.zaxis.labelpad=39
    ax.set_xlabel("$\mu$",fontsize=16, rotation=0)
    ax.set_ylabel("$\sigma^2$",fontsize=16, rotation=0)
    ax.set_zlabel("Log Max Likelihood",rotation=0)
    #ax.azim = 225
    plt.tight_layout()
    plt.show()
    return

#--------------------------------------------------------------------------------

def ML_numerical_Gauss(data,mu_min,mu_max,sigma_sq_min,sigma_sq_max,resolution_mu,resolution_sigma_sq):
    
    """ numerically computes the MLEs for mu and sigma^2 for a given 1-D Gaussian data sample on
        a predefined grid.
    
    Input:  data ... an array of 1-dimensional Gaussian distributed data points
            mu_min ... lower boundary of the grid on the mu-axis
            mu_max ... upper boundary of the grid on the mu-axis
            sigma_sq_min ... lower boundary of the grid on the sigma^2-axis
            sigma_sq_max ... upper boundary of the grid on the sigma^2-axis
            resolution_mu ... interval length between discretized points on the mu-axis
            resolution_sigma_sq ... interval length between discretized points on the sigma^2-axis
            
    Output: ML_num_Gauss ... the numerical maximum likelihood estimators for mu and sigma^2 for a Gaussian data
                       array
    """
    ML_num_Gauss = np.zeros([2,1])
    mu_arr = np.arange(mu_min,mu_max,resolution_mu)
    var_arr = np.arange(sigma_sq_min,sigma_sq_max,resolution_sigma_sq)
    mu_array,var_array = np.meshgrid(mu_arr,var_arr)

    data_len = len(data)

    sum_array = []
    for tmp_mu in mu_array[1,0:]:
        sum_array.append(np.sum((data-tmp_mu)**2))
    sum_arr = np.tile(sum_array,(len(var_arr),1))

    ML_val = data_len * np.log(1) - data_len / 2 * np.log(2 * np.pi * var_array) - 1 / 2 * np.divide(sum_arr,var_array)


    
    idx_max = np.where(ML_val==ML_val.max())
    ML_num_Gauss = [mu_arr[idx_max[0]] ,var_arr[idx_max[1]]]
    print(f"numerical mu: {ML_num_Gauss[0]}\nnumerical var: {ML_num_Gauss[1]}")
    return ML_num_Gauss
    
#--------------------------------------------------------------------------------

def plot_likelihood_Exp(data,lambda_min,lambda_max,resolution_lambda):
    
    """ Plots the joint likelihood function for lambda for a given 1-D exponentially distributed data sample on
        a predefined grid.
    
    Input:  data ... an array of 1-dimensional Gaussian distributed data points
            lambda_min ... lower boundary of the grid on the lambda-axis
            lambda_max ... upper boundary of the grid on the lambda-axis
            resolution_lambda ... interval length between discretized points on the lambda-axis
            
    Output: ---
    """
    
    lambda_array = np.arange(lambda_min,lambda_max,resolution_lambda)
    data_len = len(data)
    ML_val = data_len* np.log(lambda_array) - lambda_array * sum(data)

    plt.plot(lambda_array,ML_val)
    plt.title("Log Max Likelihood for different lambda values")
    plt.xlabel("lambda values")
    plt.ylabel("Log Max likelihood value")
    plt.grid()
    plt.show()
    return

#--------------------------------------------------------------------------------

def ML_numerical_Exp(data,lambda_min,lambda_max,resolution_lambda):
    
    """ numerically computes the MLEs for lambda for a given 1-D exponentially distributed data sample on
        a predefined grid.
    
    Input:  data ... an array of 1-dimensional exponentially distributed data points
            lambda_min ... lower boundary of the grid on the lambda-axis
            lambda_max ... upper boundary of the grid on the lambda-axis
            resolution_lambda ... interval length between discretized points on the lambda-axis
            
    Output: ML_num_Exp ... the numerical maximum likelihood estimators for mu and sigma^2 for a Gaussian data
                       array
    """
    
    ML_num_Exp = np.zeros([1,1])
    
    lambda_array = np.arange(lambda_min,lambda_max,resolution_lambda)
    data_len = len(data)
    ML_val = data_len* np.log(lambda_array) - lambda_array * sum(data)

    # TODO Compute the values of the joint exponential likelihood w.r.t. lambda and the data on a discretized 1-D grid
    #      and take the maximizing argument lambda* as the numerical MLE.
    ML_num_Exp = lambda_array[np.argmax(ML_val)]
    print(f"Numerical computed lambda value: {ML_num_Exp}\n")
    return ML_num_Exp
    
#--------------------------------------------------------------------------------

def posterior_mean_Gauss(data,mu_prior,mu_ML,sigma_sq_prior,sigma_sq_true):
    
    """ computes the parameters of the posterior distribution for the mean with a Gaussian prior and a Gaussian likelihood
    
    Input:  data ... an array of 1-dimensional Gaussian distributed data points
            mu_prior ... mean of the prior distribution of mu
            mu_ML ... maximum likelihood estimator of mu w.r.t. a certain data array
            sigma_sq_prior ... variance of the prior distribution of mu
            sigma_sq_true ... true variance of the distribution that underlies a certain data array   
            
    Output: posterior_param_Gauss ... the parameters of a posterior Gaussian distribution for the mean
    """
    
    posterior_param_Gauss = np.zeros([2,1])
    


    data_len = len(data)

    posterior_param_Gauss[0] = (sigma_sq_true *mu_prior + data_len*sigma_sq_prior*mu_ML)/(data_len*sigma_sq_prior + sigma_sq_true)

    posterior_param_Gauss[1] = 1/(1/sigma_sq_prior + data_len/sigma_sq_true)
    print(f"Posterior mu: { posterior_param_Gauss[0]}\nPosterior var: { posterior_param_Gauss[1]}\n")
    
    return posterior_param_Gauss
   
#--------------------------------------------------------------------------------
    
#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
    
