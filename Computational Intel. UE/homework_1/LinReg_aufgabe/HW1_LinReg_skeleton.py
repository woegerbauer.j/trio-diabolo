#Filename: HW1_LinReg_skeleton.py
#Author: Harald Leisenberger
#Edited: March, 2023

import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import scipy.stats as stats
from numpy.linalg import inv

#--------------------------------------------------------------------------------
# Assignment 1 - Section 3
#--------------------------------------------------------------------------------

def main():    
    
    
    # !!! All undefined functions should be implemented in the section 'Helper Functions' !!!
    
    
    # Load the two data arrays (training set: 30 x 2 - array, test set: 21 x 2 - array)
    data_train = np.loadtxt('HW1_LinReg_train.data')
    data_test = np.loadtxt('HW1_LinReg_test.data')
    
    
    # 3.2 Linear Regression with Polynomial Features
    # ----------------------------------------------
    
    ## Fit polynomials of degree D in {1,2,...,20} to the training data.
    # TODO: for D = 1,...,20 create the design matrices and fit the models
    degrees = np.linspace(1,20,20)
    Phi_list = []
    w_star_list = []
    for degree in degrees:
        Phi_train_D = design_matrix(data_train,degree)
        w_train_D = opt_weight(data_train,Phi_train_D)
        Phi_list.append(Phi_train_D)
        w_star_list.append(w_train_D)
        
    # TODO: for D = 1,2,9,16 plot the models on [-1,1]. More concretely:
    # TODO: plot the models (continuous plots) and compare them to the training targets (scatter points)
    # TODO: plot the models (continuous plots) and compare them to the test targets (scatter points)
    # For evaluating the y values of a certain model in the values x, you should use the helper function y_predict.
    
    D = [1,2,9,16]
    f1 = plt.figure(1)
    #f2 = plt.figure(2)
    delta = 0.001
    x_value = np.arange(-1,1+delta,delta)
    x_value = x_value.T

    #defining the plot
    cols = ["Train Data", "Test Data"]
    rows = ["Degree {}".format(row) for row in D ]

    fig, axes = plt.subplots(nrows=4, ncols=2, figsize=(12, 8))

    for ax, col in zip(axes[0], cols):
        ax.set_title(col)

    for ax, row in zip(axes[:,0], rows):
        ax.set_ylabel(row, rotation=90,size="large")

    n=0
    for d in D:
        y = y_predict(w_star_list[d-1], x_value)
        axes[n,0].plot(x_value,y,c='k')
        axes[n,0].scatter(data_train[:,0],data_train[:,1],marker='x',c='r')
        axes[n,0].grid(True, color = "grey", linewidth = "1.4", linestyle = "-.")
        axes[n,1].plot(x_value,y,c='b')
        axes[n,1].scatter(data_test[:,0],data_test[:,1],marker='x',c='g')
        axes[n,1].grid(True, color = "grey", linewidth = "1.4", linestyle = "-.")
        n += 1
    axes[3,0].set_xlabel("Data points",size="large")
    axes[3,1].set_xlabel("Data points",size="large")
    fig.tight_layout()
    plt.savefig("01.pdf", format="pdf", bbox_inches="tight")
    plt.show()
    
    # Compute the training and test errors of your models and plot the errors for D in {1,2,...,20}
    error_test_D = []
    error_train_D = []
    for d in range(20):
        error_train_D.append(sum_of_squared_errors(data_train,w_star_list[d]))
        error_test_D.append(sum_of_squared_errors(data_test,w_star_list[d]))
    # TODO: plot the training / test errors against the degree of the polynomial.
    plt.figure(figsize=(8, 6))
    plt.plot(degrees,np.array(error_train_D))
    plt.plot(degrees,np.array(error_test_D))
    plt.grid(True, color = "grey", linewidth = "1.4", linestyle = "-.")
    plt.ylabel("Evaluated Error",size="large") 
    plt.xlabel("Degree",size="large")
    plt.legend(["Train Data","Test Data"])
    plt.title("Error of Models")
    plt.tight_layout()
    plt.savefig("01_error.pdf", format="pdf", bbox_inches="tight")
    
    
    # Switch the role of the training data set and the test data set and repeat all of the previous steps.
    # The main difference is now to train the weight vectors on the test data.
    
    # TODO
    Phi_listS = []
    w_star_listS = []
    for degree in degrees:
        Phi_train_D = design_matrix(data_test,degree)
        w_train_D = opt_weight(data_test,Phi_train_D)
        Phi_listS.append(Phi_train_D)
        w_star_listS.append(w_train_D)
        
    # TODO: for D = 1,2,9,16 plot the models on [-1,1]. More concretely:
    # TODO: plot the models (continuous plots) and compare them to the training targets (scatter points)
    # TODO: plot the models (continuous plots) and compare them to the test targets (scatter points)
    # For evaluating the y values of a certain model in the values x, you should use the helper function y_predict.
    
    D = [1,2,9,16]
    f3 = plt.figure(3)
    f4 = plt.figure(4)
    delta = 0.001
    x_value = np.arange(-1,1+delta,delta)
    x_value = x_value.T
    
    cols = ["Train Data Switched", "Test Data  Switched"]
    rows = ["Degree {}".format(row) for row in D ]

    fig, axes = plt.subplots(nrows=4, ncols=2, figsize=(12, 8))

    for ax, col in zip(axes[0], cols):
        ax.set_title(col)

    for ax, row in zip(axes[:,0], rows):
        ax.set_ylabel(row, rotation=90,size="large")

    n=0
    for d in D:
        y = y_predict(w_star_listS[d-1], x_value)
        axes[n,0].plot(x_value,y,c='k')
        axes[n,0].scatter(data_test[:,0],data_test[:,1],marker='x',c='r')
        axes[n,0].grid(True, color = "grey", linewidth = "1.4", linestyle = "-.")
        axes[n,1].plot(x_value,y,c='b')
        axes[n,1].scatter(data_train[:,0],data_train[:,1],marker='x',c='g')
        axes[n,1].grid(True, color = "grey", linewidth = "1.4", linestyle = "-.")
        n += 1
    axes[3,0].set_xlabel("Data points",size="large")
    axes[3,1].set_xlabel("Data points",size="large")
    fig.tight_layout()
    plt.savefig("02.pdf", format="pdf", bbox_inches="tight")

    
    # Compute the training and test errors of your models and plot the errors for D in {1,2,...,20}
    error_test_D = []
    error_train_D = []
    for d in range(20):
        error_train_D.append(sum_of_squared_errors(data_train,w_star_listS[d]))
        error_test_D.append(sum_of_squared_errors(data_test,w_star_listS[d]))
    # TODO: plot the training / test errors against the degree of the polynomial.
    plt.figure(figsize=(8, 6))
    plt.plot(degrees,np.array(error_train_D))
    plt.plot(degrees,np.array(error_test_D))
    
    plt.grid(True, color = "grey", linewidth = "1.4", linestyle = "-.")
    plt.ylabel("Evaluated Error",size="large") 
    plt.xlabel("Degree",size="large")
    plt.legend(["Train Data","Test Data"])
    plt.title("Error with switched roles")
    plt.ylim(0,4)
    plt.tight_layout()
    plt.savefig("02_error.pdf", format="pdf", bbox_inches="tight")
    
    
    # Repeat the tasks from the first two steps (with the original roles of training and test data again), 
    # but now by make use of the regularized cost function:
    lambda_reg = 0.000001
    
    
    # Fit polynomials of degree D in {1,2,...,20} to the training data.
    w_star_reg_list = []
    for degree in degrees:
        Phi_train_D = design_matrix(data_train,degree)
        w_train_reg_D = opt_weight_reg(data_train,Phi_train_D,lambda_reg)
        w_star_reg_list.append(w_train_reg_D)
    
    # TODO: for D = 1,2,9,16 plot the models on [-1,1]. More concretely:
    # TODO: plot the models (continuous plots) and compare them to the training targets (scatter points)
    # TODO: plot the models (continuous plots) and compare them to the test targets (scatter points)
    # For evaluating the y values of a certain model in the values x, you should use the helper function y_predict.
    
    D = [1,2,9,16]
    f5 = plt.figure(5)
    f6 = plt.figure(6)
    delta = 0.001
    x_value = np.arange(-1,1+delta,delta)
    x_value = x_value.T
    cols = ["Train Data with Regularization", "Test Data  with Regularization"]
    rows = ["Degree {}".format(row) for row in D ]

    fig, axes = plt.subplots(nrows=4, ncols=2, figsize=(12, 8))

    for ax, col in zip(axes[0], cols):
        ax.set_title(col)

    for ax, row in zip(axes[:,0], rows):
        ax.set_ylabel(row, rotation=90,size="large")

    n=0
    for d in D:
        y = y_predict(w_star_reg_list[d-1], x_value)
        axes[n,0].plot(x_value,y,c='k')
        axes[n,0].scatter(data_train[:,0],data_train[:,1],marker='x',c='r')
        axes[n,0].grid(True, color = "grey", linewidth = "1.4", linestyle = "-.")
        axes[n,1].plot(x_value,y,c='b')
        axes[n,1].scatter(data_test[:,0],data_test[:,1],marker='x',c='g')
        axes[n,1].grid(True, color = "grey", linewidth = "1.4", linestyle = "-.")
        n += 1
    axes[3,0].set_xlabel("Data points",size="large")
    axes[3,1].set_xlabel("Data points",size="large")
    fig.tight_layout()
    plt.savefig("03.pdf", format="pdf", bbox_inches="tight")



    
    # Compute the regularized training and test errors of your models and plot the errors for D in {1,2,...,20}
    reg_error_train_D = reg_sum_of_squared_errors(data_train,w_train_reg_D,lambda_reg)
    reg_error_test_D = reg_sum_of_squared_errors(data_test,w_train_reg_D,lambda_reg)
    # TODO: plot the regularized training / test errors against the degree of the polynomial.
    reg_error_test_D = []
    reg_error_train_D = []
    for d in range(20):
        reg_error_train_D.append(reg_sum_of_squared_errors(data_train,w_star_list[d],lambda_reg))
        reg_error_test_D.append(reg_sum_of_squared_errors(data_test,w_star_list[d],lambda_reg))
    # TODO: plot the training / test errors against the degree of the polynomial.
    plt.figure(figsize=(8, 6))
    plt.plot(degrees,np.array(reg_error_train_D),linewidth = "5",color="grey")
    plt.plot(degrees,np.array(reg_error_test_D),color="red",linewidth=0.8)

    plt.grid(True, color = "grey", linewidth = "1.4", linestyle = "-.")
    plt.ylabel("Evaluated Error",size="large") 
    plt.xlabel("Degree",size="large")
    plt.legend(["Train Data","Test Data"])
    plt.title("Error with Regularization")
    plt.ylim(0,4)
    plt.tight_layout()
    plt.savefig("03_error.pdf", format="pdf", bbox_inches="tight")
    
    # 3.3 Linear Regression with Sigmoidal Features
    # ----------------------------------------------
    
    # Repeat the first two steps from 3.2, but now use sigmoidal basis functions. Use the original error function.
    # TODO: The main difference is the construction of the design matrix based on sigmoidal basis functions.
    #       Therefore, implement a helper function design_matrix_sigmoid(data,nr_basis_functions,x_min,x_max)
    
    
    pass
    
    
#--------------------------------------------------------------------------------
# Helper Functions (to be implemented!)
#--------------------------------------------------------------------------------

    
def design_matrix(data,degree):
    
    """ computes the design matrix for given data and polynomial basis functions up to a certain degree
    
    Input:  data ... an N x 2 array (1st column: feature values; 2st column: targets)
            degree ... maximum degree D of the polynomial basis function.
            
    Output: Phi ... the design matrix (has to be a N x (D + 1) - array)
    """
    data_len = int(len(data))
    degree = int(degree)
    
    dummy_vec = np.ones([data_len,degree]);
    setup_mx = dummy_vec * data[:,[0]]

    basis_mx = np.cumprod(setup_mx,axis=1)
    Phi = np.append(np.ones([data_len,1]),basis_mx,axis=1)
    return Phi

#--------------------------------------------------------------------------------

def design_matrix_sigmoid(data,nr_basis_functions,x_min,x_max):

    """ computes the design matrix for given data and polynomial basis functions up to a certain degree
    
    Input:  data ... an N x 2 array (1st column: feature values; 2st column: targets)
            nr_basis_functions ... number D + 1 of the sigmoidal basis function.
            x_min ... lower bound of the x range interval
            x_max ... upper bound of the x range interval
            
    Output: Phi_sig ... the design matrix (has to be a N x (D + 1) - array)
    """
    
    Phi_sig = np.zeros([len(data),nr_basis_functions]) 
    
    # TODO: Create the design matrix for a given data array and a fixed number of sigmoidal basis function.
    #       First, the centers and the width of the basis functions must be specified, according to the
    #       range [x_min,x_max] to be considered for model fitting.
    D = nr_basis_functions
    data_len = int(len(data))
    s = 2/D
    x = np.random.uniform(low=-1, high=1, size=data_len)
    
    dummy_vec = np.ones([data_len,D]);
    setup_mx = dummy_vec * sigmoid(x,data,s)
    
    
    basis_mx = np.cumprod(setup_mx,axis=1)
    Phi = np.append(np.ones([data_len,1]),basis_mx,axis=1)

    return Phi_sig

def sigmoid(x,c,s):
    sigma = 1/(1+np.exp(-(x-c)/s))
    return sigma

#--------------------------------------------------------------------------------


def opt_weight(data,Phi):

    """ computes the optimal weight vector for a given design matrix and the corresponding targets.
    
    Input: data ... N x 2 array (1st column: feature values; 2st column: targets)
           Phi ... the design matrix (N x (D + 1)) that corresponds to the data
    
    Output: w_star ... the optimal weight vector ((D + 1) x 1)
    """
    
    w_star = np.zeros([len(Phi.T),1])
    
    # TODO Compute the optimal weight vector (with respect to the unmodified error function) for given
    #      targets and a given design matrix.
    w_star = np.matmul(np.matmul(inv(np.matmul(np.transpose(Phi),Phi)),np.transpose(Phi)),data[:,1])

    return w_star

#--------------------------------------------------------------------------------

def opt_weight_reg(data,Phi,lambda_reg):

    """ computes the optimal weight vector for a given design matrix and the corresponding targets, when
        considering the regularized error function with regularization parameter lambda.
    
    Input: data ... N x 2 array (1st column: feature values; 2st column: targets)
           Phi ... the design matrix (N x (D + 1)) that corresponds to the data
           lambda_reg ... the regularization parameter
    
    Output: w_star_reg ... the optimal weight vector ((D + 1) x 1)
    """
    
    w_star_reg = np.zeros([len(Phi.T),1])
    
    # TODO: Compute the optimal weight vector (with respect to the regularized error function) for given
    #       targets and a given design matrix.
    w_star_reg = np.matmul(np.matmul(inv(lambda_reg*np.eye(len(Phi.T)) + np.matmul(np.transpose(Phi),Phi)),np.transpose(Phi)),data[:,1])

    return w_star_reg

#--------------------------------------------------------------------------------

def y_predict(w_train_D,x_value):

    """ evaluates the predicted value for y of a given model defined by the corresponding weight vector
        in a given value x.
        
    Input: w_train_D ... the optimal weight vector for a polynomial model with degree D that has been
                         trained on the training data
           x_value ... an arbitrary value on the x-axis in that the y-value of the model should be computed
    
    Output: y_value ... the y-value corresponding to the given x-value for the given model
    """
    
    # TODO: Compute the predicted y-value for a given model with degree D and its weight vector w_train_D in x.
    x_value = np.array(x_value)
    
    if x_value.ndim != 1:
        x_value = x_value[:,0]
        
        
    data_len = int(len(x_value))

    degree = int(len(w_train_D))

    dummy_vec = np.ones([data_len,degree-1]);
    setup_mx = dummy_vec * np.reshape(x_value,(data_len,-1))
    poly_mx = np.cumprod(setup_mx,axis=1)
    poly_mx = np.append(np.ones([data_len,1]),poly_mx,axis=1)
    y_value =  np.matmul(poly_mx , w_train_D.T)

    return y_value

#--------------------------------------------------------------------------------

def sum_of_squared_errors(data,w_star):

    """ Computes the sum of squared errors between the values Phi*w that are predicted by a model
        and the actual targets t.
        
    Input: data ... N x 2 array (1st column: feature values; 2st column: targets)
           w_star ... the optimum weight vector corresponding to a certain model
           
    Output: error ... the sum of squared errors E(w) for a given data array (the output must be a number!)
                      and a given model (= weight vector).
    """
    x = data[:,0]
    t = data[:,1]
    
    # TODO: Evaluate the error of a model (defined by w_star) on a given data array.
    degree = len(w_star)-1
    Phi = design_matrix(data, degree)
    error = 0.5*np.matmul(np.transpose(t - np.matmul(Phi,w_star)),(t - np.matmul(Phi,w_star)))

    return error

#--------------------------------------------------------------------------------    
    
def reg_sum_of_squared_errors(data_train,w_train_reg_D,lambda_reg):

    """ Computes the sum of squared errors between the values Phi*w that are predicted by a model
        and the actual targets t.
        
    Input: data ... N x 2 array (1st column: feature values; 2st column: targets)
           w_star ... the optimum weight vector corresponding to a certain model
           lambda_reg ... the value for the regularization parameter
           
    Output: reg_error ... the value of the lambda-regularized cost function E_lambda for a given data array and
                          a given model (= weight vector).
    """

    # TODO: Evaluate the regularized error of a model (defined by w_star) on a given data array.
    reg_error = sum_of_squared_errors(data_train, w_train_reg_D) + lambda_reg*0.5*np.matmul(np.transpose(w_train_reg_D),w_train_reg_D)

    return reg_error

#--------------------------------------------------------------------------------

#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
